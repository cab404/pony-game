package com.cab404.ponies.realizations;

import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.OrderedMap;
import com.cab404.ponies.Stream;

public class Transition extends Stream {
	
	Sprite obj;
	private float w = 0, h = 0, x = 0, y = 0, rot = 0;
	private float w2, h2, x2, y2, rot2;
	private float time = 60;
	
	public Transition(OrderedMap<String, Float> in, Sprite spr) {
		Iterator<String> iter = in.keys();
		obj = spr;
		
		w = w2 = obj.getWidth();
		h = h2 = obj.getHeight();
		x = x2 = obj.getX();
		y = y2 = obj.getY();
		rot = rot2 = obj.getRotation();
		
		while (iter.hasNext()) {
			String key = iter.next();
			
			if (key == "w")
				w2 = in.get(key);
			else if (key == "h")
				h2 = in.get(key);
			else if (key == "x")
				x2 = in.get(key);
			else if (key == "y")
				y2 = in.get(key);
			else if (key == "rotation")
				rot2 = in.get(key);
			else if (key == "time") time = (int) in.get(key).floatValue();
		}
		
		w = (w2 - w) / time;
		h = (h2 - h) / time;
		x = (x2 - x) / time;
		y = (y2 - y) / time;
		rot = (rot2 - rot) / time;
		
	}
	
	@Override
	public void update() {
		if (time >= 0) {
			time--;
			obj.setX(obj.getX() + x);
			obj.setY(obj.getY() + y);
			obj.setSize(obj.getWidth() + w, obj.getHeight() + h);
			obj.rotate(rot);
		}
		else
			cancel();
	}
	
	@Override
	public void cancel() {
		isFinished = true;
		if (!Float.isNaN(x2)) obj.setX(x2);
		if (!Float.isNaN(y2)) obj.setY(y2);
		if (!Float.isNaN(w2) && !Float.isNaN(h2)) obj.setSize(w2, h2);
		if (!Float.isNaN(rot2)) obj.setRotation(rot2);
	}
}
