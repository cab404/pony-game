package com.cab404.ponies.realizations.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.MuffinData;
import com.cab404.ponies.Pony;
import com.cab404.ponies.Util;

public class RenaiFoxy extends Pony {
	
	public RenaiFoxy() {
		
		Texture reina = new Texture(Gdx.files.internal("data/images/players/renai/raita.png"));
		
		Sprite[] anima;
		anima = Util.split(reina, 0, 0, 16, 16, 9);
		
		for (Sprite sprite : anima) {
			sprite.setSize(32, 32);
		}
		
		animation = new Animation(anima, 2);
		
		Luna.toDispose.add(reina);
		
	}
	
	@Override
	public void init(Muffin mf) {
		mf.stats = MuffinData.getStandartPonyData("ReinaFoxy");
		mf.stats.canFly = false;
		mf.stats.jumpHeight = 7;
		mf.stats.additionalJumpSpeedBonusLength = 10;
		mf.stats.additionalJumpSpeed = 0.5f;
		
	}
	
	@Override
	public void onWalking(Muffin phys) {
		animation.sIndex = 0;
		animation.fIndex = 7;
		
		updFlip(phys.vX);
		
		animation.delay = 4 - (int) Math.abs(phys.vX);
		if (animation.delay < 1) animation.delay = 1;
		
		phys.stats.limitY = 15;
		animation.isPaused = false;
	}
	
	@Override
	public void onStanding(Muffin phys) {
		animation.setIndex(8);
		phys.stats.limitY = 20;
		animation.isPaused = true;
	}
	
	@Override
	public void inMidair(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(1);
		
		animation.isPaused = true;
	}
	
	@Override
	public void whileFlying(Muffin phys) {
		// Не может летать
	}
	
	@Override
	public void jumpUp(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(1);
		
		animation.isPaused = true;
		
	}
	
	@Override
	public void fallDown(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(1);
		
		animation.isPaused = true;
		
	}
	
}
