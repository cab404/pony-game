package com.cab404.ponies.realizations.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pony;
import com.cab404.ponies.MuffinData;

public class Fluttershy extends Pony {
	
	public static Texture fluttershy;
	
	public Fluttershy() {
		
		if (fluttershy == null) {
			fluttershy = new Texture(
					Gdx.files.internal("data/images/players/fluttershy/fluttershy.png"));
			Luna.toDispose.add(fluttershy);
		}
		
		Sprite[] anima = new Sprite[33];
		
		for (int i = 0; i != 17; i++)
			anima[i] = new Sprite(new TextureRegion(fluttershy, 52 * i, 0, 51, 45));
		
		for (int i = 0; i != 16; i++)
			anima[i + 17] = new Sprite(new TextureRegion(fluttershy, 52 * i, 45, 51, 45));
		
		animation = new Animation(anima, 2);
		
	}
	
	@Override
	public void init(Muffin mf) {
		mf.stats =  MuffinData.getStandartPonyData("Флаттершай");
	}
	
	@Override
	public void onWalking(Muffin phys) {
		animation.sIndex = 0;
		animation.fIndex = 15;
		
		updFlip(phys.vX);
		
		animation.delay = 4 - (int) Math.abs(phys.vX);
		if (animation.delay < 1) animation.delay = 1;
		
		phys.stats.limitY = 15;
		animation.isPaused = false;
	}
	
	@Override
	public void onStanding(Muffin phys) {
		animation.setIndex(16);
		phys.stats.limitY = 20;
		animation.isPaused = true;
	}
	
	@Override
	public void inMidair(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(15);
		
		animation.isPaused = true;
	}
	
	@Override
	public void whileFlying(Muffin phys) {
		updFlip(phys.vX);
		animation.sIndex = 17;
		animation.fIndex = 32;
		animation.delay = 4;
		phys.stats.limitY = 4;
		animation.isPaused = false;
		
	}
	
	@Override
	public void jumpUp(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(17);
		
		animation.isPaused = true;
		
	}
	
	@Override
	public void fallDown(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(18);
		
		animation.isPaused = true;
		
	}
	
}
