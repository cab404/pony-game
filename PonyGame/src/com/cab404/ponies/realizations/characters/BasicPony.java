package com.cab404.ponies.realizations.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pony;

public class BasicPony extends Pony {
	
	public static Sound h1 = null, h2;
	
	/**
	 * Creates pony object out of standard 17-tile image.
	 * 
	 * @param name
	 *            Folder name in images/players directory
	 * @param width
	 *            Width of a tile
	 * @param height
	 *            Height of a tile
	 */
	public BasicPony(String name, int width, int height) {
		
		Sprite[] toAnim = new Sprite[17];
		Texture pon4 = new Texture(Gdx.files.internal("data/images/players/" + name + "/" + name
				+ ".png"));
		
		for (int i = 0; i != 17; i++)
			toAnim[i] = new Sprite(new TextureRegion(pon4, width * i, 0, width, height));
		
		if (h1 == null) {
			h1 = Gdx.audio.newSound(Gdx.files.internal("data/sounds/h1.ogg"));
			h2 = Gdx.audio.newSound(Gdx.files.internal("data/sounds/h2.ogg"));
			Luna.disposeOnExit.add(h1);
			Luna.disposeOnExit.add(h2);
		}
		
		animation = new Animation(toAnim, 10);
	}
	
	@Override
	public void inMidair(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(5);
		animation.isPaused = true;
	}
	
	@Override
	public void onWalking(Muffin phys) {
		updFlip(phys.vX);
		animation.sIndex = 0;
		animation.fIndex = 15;
		
		if (animation.getIndex() == 4) h1.play(0.5f);
		if (animation.getIndex() == 10) h2.play(0.5f);
		
		animation.delay = 4 - (int) Math.abs(phys.vX);
		if (animation.delay < 1) animation.delay = 1;
		
		animation.isPaused = false;
	}
	
	@Override
	public void onStanding(Muffin phys) {
		animation.setIndex(16);
		animation.isPaused = true;
	}
	
	@Override
	public void whileFlying(Muffin phys) {
		// CANNOT!
	}
	
	@Override
	public void jumpUp(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(3);
		animation.isPaused = true;
	}
	
	@Override
	public void fallDown(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(7);
		animation.isPaused = true;
	}
	
}
