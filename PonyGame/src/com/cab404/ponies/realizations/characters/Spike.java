package com.cab404.ponies.realizations.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pony;
import com.cab404.ponies.Util;

public class Spike extends Pony {
	
	public Spike() {
		
		Texture spike = new Texture(Gdx.files.internal("data/images/players/spike/spike.png"));
		animation = new Animation(Util.split(spike, 0, 0, 17, 30, 13), 5);
		
	}
	
	@Override
	public void inMidair(Muffin phys) {
		animation.setIndex(5);
		animation.isPaused = true;
	}
	
	@Override
	public void onWalking(Muffin phys) {
		animation.sIndex = 0;
		animation.fIndex = 11;
		updFlip(phys.vX);
		
		animation.delay = 4 - (int) Math.abs(phys.vX);
		if (animation.delay < 1) animation.delay = 1;
		
		animation.isPaused = false;
	}
	
	@Override
	public void onStanding(Muffin phys) {
		
		animation.setIndex(12);
		animation.isPaused = true;
	}
	
	@Override
	public void whileFlying(Muffin phys) {
		
	}
	
	@Override
	public void jumpUp(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(2);
		animation.isPaused = true;
	}
	
	@Override
	public void fallDown(Muffin phys) {
		updFlip(phys.vX);
		animation.setIndex(10);
		animation.isPaused = true;
	}
	
}
