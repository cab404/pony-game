package com.cab404.ponies.realizations.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pony;
import com.cab404.ponies.Util;

public class Discord extends Pony {
	
	private boolean rFlipped = false;
	
	public Discord() {
		name = "Discord";
		
		Texture disCord = new Texture(Gdx.files.internal("data/images/players/discord/discord.png"));
		Luna.toDispose.add(disCord);
		
		Sprite[] spr = new Sprite[52];
		
		int w = 74, h = 100;
		int counter = 0;
		
		for (int y = 0; y != 4; y++)
			for (int i = 0; i != 8; i++, counter++) {
				spr[counter] = new Sprite(new TextureRegion(disCord, w * i, h * y, w, h));
				spr[counter].setSize(60, 81);
			}
		
		for (int y = 4; y != 6; y++)
			for (int i = 0; i != 10; i++, counter++) {
				spr[counter] = new Sprite(new TextureRegion(disCord, w * i, h * y, w, h));
				spr[counter].setSize(60, 81);
			}
		
		animation = new Animation(spr, 10);
		
	}
	
	@Override
	public void inMidair(Muffin phys) {
		
	}
	
	@Override
	public void onWalking(Muffin phys) {
		animation.sIndex = 16;
		animation.fIndex = 23;
		if (phys.vX > 0) {
			animation.sIndex += 8;
			animation.fIndex += 8;
			rFlipped = true;
		}
		else
			rFlipped = false;
		
		animation.delay = 4 - (int) Math.abs(phys.vX);
		if (animation.delay < 1) animation.delay = 1;
		
		animation.isPaused = false;
		bored = 600;
	}
	
	private static int bored = 600;
	
	@Override
	public void onStanding(Muffin phys) {
		animation.sIndex = 32;
		bored--;
		
		if (bored == 0) {
			Util.addMessage(phys, "I'm missing some beautiful chaos to do. ", 120);
			bored = 600;
		}
		animation.fIndex = 41;
		if (rFlipped) {
			animation.sIndex += 10;
			animation.fIndex += 10;
		}
		
		animation.delay = 3;
		
		animation.isPaused = false;
	}
	
	@Override
	public void whileFlying(Muffin phys) {
		jumpUp(phys);
	}
	
	@Override
	public void jumpUp(Muffin phys) {
		bored = 600;
		animation.setIndex(4);
		
		if (phys.vX > 0)
			rFlipped = true;
		else if (phys.vX < 0) rFlipped = false;
		
		animation.setIndex(rFlipped ? 12 : 4);
		
		animation.isPaused = true;
	}
	
	@Override
	public void fallDown(Muffin phys) {
		jumpUp(phys);
	}
}
