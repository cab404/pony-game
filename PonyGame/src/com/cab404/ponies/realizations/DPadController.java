package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.OrderedMap;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Util;

public class DPadController extends Controller {
	public int touchRadius = (int) Util.ppc(1);
	int w = Gdx.graphics.getWidth(), h = Gdx.graphics.getHeight();
	int rx = w - (int) Util.ppc(2), ry = (int) Util.ppc(2); // X
	private Inventory currentlyOpenedInventoryWindow;
	public Sprite bg, handle, jump, fly, tab, inv;
	public boolean wasTouched = false, isUpLocked = false, wasTouchedFly = false, isFlying = false;
	private boolean isTabLocked = false;
	private int leftAdditionalJumpingSpeed = 0;
	int touchIndex = 0, transition = -1;
	
	public Vector2 getCoords(int x, int y) {
		y = h - y;
		x -= rx;
		y -= ry;
		
		Vector2 toret = new Vector2(x, y).nor();
		
		float dst = (float) Math.sqrt(x * x + y * y);
		
		if (dst < touchRadius) {
			toret.x = toret.x * dst / touchRadius;
			toret.y = toret.y * dst / touchRadius;
		}
		
		return toret;
	}
	
	public void rearrange() {
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();
		// float pr = Gdx.graphics.getDensity();
		int rw = (int) Util.ppc(2), rh = (int) Util.ppc(2);
		rx = (int) (w - Util.ppc(2));
		ry = (int) Util.ppc(2);
		
		Luna.gui.removeAll(new Array<Sprite>(new Sprite[] { bg, handle, fly, jump, tab, inv }),
				false);
		
		bg.setSize(rw, rh);
		jump.setSize(rw / 3, rh / 3);
		fly.setSize(rw / 3, rh / 3);
		handle.setSize(rw / 3, rh / 3);
		tab.setSize(rw / 3, rh / 3);
		inv.setSize(rw / 3, rh / 3);
		
		bg.setPosition(rx - rw / 2, ry - rh / 2);
		handle.setPosition(rx - rw / 6, ry - rh / 6);
		jump.setPosition(w / 11f, ry - rh / 6 + jump.getHeight());
		fly.setPosition(w / 11f, ry - rh / 6 - fly.getHeight());
		tab.setPosition(w / 11f, h - fly.getX());
		inv.setPosition(w - tab.getX(), tab.getY());
		
		Luna.gui.addAll(new Sprite[] { bg, handle, fly, jump, tab, inv });
	}
	
	public DPadController() {
		identity = "DPad";
		Texture tex = new Texture(Gdx.files.internal("data/images/d-pad.png"));
		Luna.toDispose.add(tex);
		
		fly = new Sprite(new TextureRegion(tex, 0, 20, 20, 20));
		jump = new Sprite(new TextureRegion(tex, 20, 0, 20, 20));
		tab = new Sprite(new TextureRegion(tex, 40, 0, 20, 20));
		handle = new Sprite(new TextureRegion(tex, 0, 0, 20, 20));
		bg = new Sprite(new TextureRegion(tex, 20, 20, 44, 44));
		inv = new Sprite(new TextureRegion(tex, 0, 40, 20, 20));
		
		rearrange();
	}
	
	@Override
	public float[] getMovement(Muffin phys) {
		float[] toret = new float[] { 0, 0, 0 };
		Vector2 out = new Vector2(0, 0);
		isFlying = phys.isFlying;
		
		// Достаём данные с джойстика
		
		try {
			if ((wasTouched && Gdx.input.isTouched(touchIndex)) | Util.isPressed(handle)) {
				if (Util.isPressed(handle)) touchIndex = Util.getPressed(handle);
				
				out = getCoords(Gdx.input.getX(touchIndex), Gdx.input.getY(touchIndex));
				
				toret[0] = (out.x * phys.stats.limitX);
				
				if (phys.isFlying) toret[1] = (int) (out.y * phys.stats.limitX);
				
				wasTouched = true;
			}
			else {
				wasTouched = false;
				touchIndex = 0;
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			touchIndex = 0;
		}
		
		// Кнопка прыжка
		
		if (Util.isPressed(jump) || out.y > 0.5f) {
			if (!isUpLocked && !phys.isFalling) {
				toret[1] += phys.stats.jumpHeight;
				
				leftAdditionalJumpingSpeed = phys.stats.additionalJumpSpeedBonusLength;
				isUpLocked = true;
			}
			else if (phys.isFalling && leftAdditionalJumpingSpeed != 0) {
				toret[1] += phys.stats.additionalJumpSpeed;
				
				leftAdditionalJumpingSpeed--;
			}
		}
		else {
			isUpLocked = false;
			leftAdditionalJumpingSpeed = 0;
		}
		
		if (!phys.isFalling) isUpLocked = false;
		
		if (!phys.stats.canFly) {
			fly.setSize(0, 0);
			jump.setSize(Util.ppc(1), Util.ppc(1));
			jump.setPosition(Util.ppc(1), Util.ppc(1));
			isFlying = false;
		}
		else
			fly.setSize(tab.getWidth(), tab.getHeight());
		
		if (Util.isPressed(fly)) {
			if (!wasTouchedFly) {
				wasTouchedFly = true;
				isFlying = !isFlying;
				OrderedMap<String, Float> mp = new OrderedMap<String, Float>();
				
				if (!isFlying) {
					mp.put("w", tab.getWidth());
					mp.put("h", tab.getHeight());
					mp.put("x", w / 11f);
					mp.put("y", ry - h / 7 * 2 / 6 + tab.getHeight());
				}
				else {
					mp.put("w", 0f);
					mp.put("h", 0f);
					mp.put("x", jump.getX() + tab.getWidth() / 2);
					mp.put("y", jump.getY() + tab.getHeight() / 2);
				}
				mp.put("time", 15f);
				Util.cancelStream(transition);
				transition = Util.performTransition(mp, jump);
			}
		}
		else
			wasTouchedFly = false;
		
		fly.setColor(1, 1, 1, 1);
		if (isFlying) fly.setColor(0.5f, 1, 0.5f, 1);
		
		if (Util.isPressed(tab)) {
			if (!isTabLocked) {
				isTabLocked = true;
				Util.chChar();
			}
		}
		else
			isTabLocked = false;
		
		if (Util.isPressed(inv)) {
			currentlyOpenedInventoryWindow = new Inventory(phys);
			Luna.overlays.add(currentlyOpenedInventoryWindow);
		}
		
		toret[2] = isFlying ? 1 : 0;
		
		handle.setPosition(out.x * touchRadius - handle.getWidth() / 2 + rx, out.y * touchRadius
				- handle.getHeight() / 2 + ry);
		
		return toret;
	}
	
	@Override
	public void dispose() {
		Luna.gui.removeAll(new Array<Sprite>(new Sprite[] { bg, handle, fly, jump, tab, inv }),
				false);
		if (currentlyOpenedInventoryWindow != null)
			currentlyOpenedInventoryWindow.isMarkedToBeDeleted = true;
	}
	
}
