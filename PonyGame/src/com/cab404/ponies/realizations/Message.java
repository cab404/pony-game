package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pinnable;
import com.cab404.ponies.Util;

/** Text message. Can be pinned to PBody object **/

public class Message extends Pinnable {
	
	public static Sprite[] boxCorner, boxVert;
	public static Sprite boxInner;
	public static boolean isInited = false;
	public TextBounds bb;
	
	/** Size of element **/
	public static float elS = 8;
	/** Minimal height of message box **/
	public static int minHeight = (int) (Luna.pubf.getCapHeight() * Luna.cam.zoom);
	public String textToDraw;
	public static BitmapFont fnt;
	
	public Message(String inp) {
		
		textToDraw = inp;
		if (!isInited) {
			isInited = true;
			
			int size = (int) (Gdx.app.getVersion() == 0 ? Util.ppc(0.3f) : Util.ppc(0.2f));
			
			FreeTypeFontGenerator ftb = new FreeTypeFontGenerator(
					Gdx.files.internal("data/font/ubuntu.ttf"));
			Message.fnt = ftb.generateFont(size, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
					+ "abcdefghijklmnopqrstuvwxyz" + "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
					+ "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
					+ "1234567890-=!\"№;%:?*()_+\\/|<>.,'[]{}", false);
			fnt.setUseIntegerPositions(true);
			ftb.dispose();
			
			Texture tex = new Texture(Gdx.files.internal("data/images/dialogBox.png"));
			
			Luna.disposeOnExit.add(tex);
			Luna.disposeOnExit.add(fnt);
			
			boxCorner = new Sprite[4];
			boxVert = new Sprite[4];
			
			boxInner = new Sprite(
					new TextureRegion(tex, (int) elS, (int) elS, (int) elS, (int) elS));
			
			boxVert[0] = new Sprite(new TextureRegion(tex, 0, 0, (int) elS, (int) elS));
			boxVert[1] = new Sprite(
					new TextureRegion(tex, 0, (int) (elS * 2), (int) elS, (int) elS));
			boxVert[2] = new Sprite(new TextureRegion(tex, (int) (elS * 2), (int) (elS * 2),
					(int) elS, (int) elS));
			boxVert[3] = new Sprite(
					new TextureRegion(tex, (int) (elS * 2), 0, (int) elS, (int) elS));
			
			boxCorner[0] = new Sprite(new TextureRegion(tex, 0, (int) elS, (int) elS, (int) elS));
			boxCorner[1] = new Sprite(new TextureRegion(tex, (int) elS, 0, (int) elS, (int) elS));
			boxCorner[2] = new Sprite(new TextureRegion(tex, (int) (elS * 2), (int) elS, (int) elS,
					(int) elS));
			boxCorner[3] = new Sprite(new TextureRegion(tex, (int) elS, (int) (elS * 2), (int) elS,
					(int) elS));
			
		}
	}
	
	@Override
	public void draw(Muffin pon, SpriteBatch btch) {
		String toDraw = textToDraw;
		int windowWidth = (int) Util.ppc(3);
		
		float w = pon.w * Luna.charBzoom;
		float h = pon.h * Luna.charBzoom;
		float x = pon.x * Luna.charBzoom;
		float y = pon.y * Luna.charBzoom + h;
		
		TextBounds tmp = fnt.getWrappedBounds(toDraw,
				Math.max(windowWidth - (int) (elS * 2), fnt.getBounds(pon.pony.name).width)
						+ (int) (elS * 2));
		tmp.height = tmp.height < minHeight ? minHeight : tmp.height;
		
		int bw = (int) Math.max(tmp.width, fnt.getBounds(pon.pony.name).width);
		tmp = fnt.getWrappedBounds(toDraw, bw);
		int bh = (int) (tmp.height + fnt.getCapHeight() + fnt.getXHeight());
		
		if (pon.pony.animation.isFlipped) x += bw + elS * 2 + w;
		
		Vector2 dot1 = new Vector2(x - bw - (int) (elS * 2), y + bh + (int) elS);
		Vector2 dot2 = new Vector2(x - bw - (int) (elS * 2), y);
		Vector2 dot3 = new Vector2(x - (int) elS, y);
		Vector2 dot4 = new Vector2(x - (int) elS, y + bh + (int) elS);
		
		btch.draw(boxVert[0], dot1.x, dot1.y);
		btch.draw(boxVert[1], dot2.x, dot2.y);
		btch.draw(boxVert[2], dot3.x, dot3.y);
		btch.draw(boxVert[3], dot4.x, dot4.y);
		
		btch.draw(boxCorner[1], dot1.x + (int) elS, dot1.y, bw, (int) elS);
		btch.draw(boxCorner[2], dot4.x, dot3.y + (int) elS, (int) elS, bh);
		btch.draw(boxCorner[3], dot2.x + (int) elS, dot2.y, bw, (int) elS);
		btch.draw(boxCorner[0], dot2.x, dot2.y + (int) elS, (int) elS, bh);
		
		btch.draw(boxInner, dot2.x + (int) elS, dot2.y + (int) elS, bw, bh);
		
		fnt.setColor(Color.toFloatBits(160, 50, 30, 255));
		fnt.drawWrapped(btch, pon.pony.name, x - bw - (int) elS, y + bh + (int) elS, bw);
		fnt.setColor(Color.toFloatBits(46, 46, 37, 255));
		bb = fnt.drawWrapped(btch, toDraw, x - bw - (int) elS,
				y + bh + (int) elS - fnt.getCapHeight() - fnt.getXHeight(), bw);
	}
}