package com.cab404.ponies.realizations;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.cab404.ponies.Animation;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pony;

public class Item extends Muffin {
	
	/**
	 * Pony, who grabs it.
	 */
	public Muffin owner;
	public Sprite icon;
	public String description;
	public int sW, sH;
	
	public Item(Pony reg) {
		super(reg, Controller.getNilController());
		stats.isNPC = true;
		icon = reg.animation.frames[0];
		sW = reg.animation.getWidth();
		sH = reg.animation.getHeight();
	}
	
	public static Pony getStaticAnim(final Sprite spr) {
		return new Pony() {
			{
				animation = new Animation(new Sprite[] { spr }, 0);
				animation.isPaused = true;
			}
			
			@Override
			public void init(Muffin in) {
				in.stats.isNPC = true;
			}
			
			@Override
			public void inMidair(Muffin phys) {}
			
			@Override
			public void onWalking(Muffin phys) {}
			
			@Override
			public void onStanding(Muffin phys) {}
			
			@Override
			public void whileFlying(Muffin phys) {}
			
			@Override
			public void jumpUp(Muffin phys) {}
			
			@Override
			public void fallDown(Muffin phys) {}
		};
	}
	
	@Override
	public void onUpdate() {
		grab();
		
		if (owner == null) {
			icon.setSize(sW, sH);
			w = sW;
			h = sH;
			isFalling = true;
			for (Muffin item : Luna.chars)
				if (!Item.class.isInstance(item) && getBounds().overlaps(item.getBounds()))
					for (int i = 0; i != item.nowGrabbing.length; i++) {
						boolean upBreak = false;
						for (int j = 0; j != item.nowGrabbing.length; j++)
							if (item.nowGrabbing[i][j] == null) {
								item.nowGrabbing[i][j] = this;
								owner = item;
								upBreak = true;
								break;
							}
						if (upBreak) break;
					}
			
		}
		
	}
	
	/**
	 * Invoke continuously
	 * 
	 * @param carrier
	 */
	public void grab() {
		if (owner != null) {
			x = owner.x - (owner.pony.animation.isFlipped ? -owner.w - 10 : w + 10);
			y = owner.y + owner.w / 2;
			vX = owner.vX;
			vY = owner.vY;
		}
	}
	
	@Override
	public void draw() {
		if (owner == null) pony.animation.draw(Luna.charB, x, y);
	}
	
}
