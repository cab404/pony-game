package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.cab404.ponies.KV;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pinnable;
import com.cab404.ponies.Util;

public class Choise extends Pinnable {
	
	public static Sprite[] boxCorner, boxVert;
	public static Sprite boxInner;
	public static Sprite button;
	public static boolean isInited = false;
	private boolean tapped = false;
	private KV<String, Runnable>[] choises;
	private String question;
	/**
	 * Размер элемента в пикселях. Элементом считаются углы, кромки и фон
	 * окошка.
	 **/
	public static int elS = 8;
	
	public Choise(String question, KV<String, Runnable>[] choises) {
		this.question = question;
		this.choises = choises;
		
		if (!isInited) {
			isInited = true;
			if (!Message.isInited) new Message("");
			
			Texture tex = new Texture(Gdx.files.internal("data/images/pause.png"));
			
			Luna.disposeOnExit.add(tex);
			
			boxCorner = new Sprite[4];
			boxVert = new Sprite[4];
			
			boxInner = new Sprite(new TextureRegion(tex, elS, elS, elS, elS));
			
			boxVert[0] = new Sprite(new TextureRegion(tex, 0, 0, elS, elS));
			boxVert[1] = new Sprite(new TextureRegion(tex, 0, elS * 2, elS, elS));
			boxVert[2] = new Sprite(new TextureRegion(tex, elS * 2, elS * 2, elS, elS));
			boxVert[3] = new Sprite(new TextureRegion(tex, elS * 2, 0, elS, elS));
			
			boxCorner[0] = new Sprite(new TextureRegion(tex, 0, elS, elS, elS));
			boxCorner[1] = new Sprite(new TextureRegion(tex, elS, 0, elS, elS));
			boxCorner[2] = new Sprite(new TextureRegion(tex, elS * 2, elS, elS, elS));
			boxCorner[3] = new Sprite(new TextureRegion(tex, elS, elS * 2, elS, elS));
			
			button = new Sprite(new TextureRegion(tex, 0, 24, 64, 19));
			
		}
		
	}
	
	@Override
	public void draw(Muffin pon, SpriteBatch btch) {
		
		int distanceBetweenButtons = (int) Util.ppc(0.1f);
		int windowWidth = (int) (button.getWidth() / button.getHeight()
				* Message.fnt.getCapHeight() * 4)
				- elS * 2;
		int buttonHeight = (int) (windowWidth / (button.getWidth() / button.getHeight()));
		int wholeHeight = (int) (choises.length * (buttonHeight + distanceBetweenButtons)
				+ distanceBetweenButtons + Message.fnt.getWrappedBounds(question, windowWidth).height);
		
		float x = pon.x, y = pon.y + pon.h;
		int bw = windowWidth; // Размер окна без рамки
		int bh = wholeHeight;
		
		if (pon.pony.animation.isFlipped) x += bw + elS * 2 + pon.w;
		
		Vector2 dot1 = new Vector2(x - bw - elS * 2, y + bh + elS);
		Vector2 dot2 = new Vector2(x - bw - elS * 2, y);
		Vector2 dot3 = new Vector2(x - elS, y);
		Vector2 dot4 = new Vector2(x - elS, y + bh + elS);
		
		btch.draw(boxVert[0], dot1.x, dot1.y);
		btch.draw(boxVert[1], dot2.x, dot2.y);
		btch.draw(boxVert[2], dot3.x, dot3.y);
		btch.draw(boxVert[3], dot4.x, dot4.y);
		
		btch.draw(boxCorner[1], dot1.x + elS, dot1.y, bw, elS);
		btch.draw(boxCorner[2], dot4.x, dot3.y + elS, elS, bh);
		btch.draw(boxCorner[3], dot2.x + elS, dot2.y, bw, elS);
		btch.draw(boxCorner[0], dot2.x, dot2.y + elS, elS, bh);
		
		btch.draw(boxInner, dot2.x + (int) elS, dot2.y + (int) elS, bw, bh);
		
		int startPointX = (int) (x - elS - bw);
		int startPointY = (int) (y + bh - elS - Message.fnt.getWrappedBounds(question, bw).height)
				- distanceBetweenButtons;
		
		Message.fnt.drawWrapped(btch, question, startPointX, y + bh + elS, bw);
		
		for (int i = 0; i != choises.length; i++) {
			
			int buttonY = (int) (startPointY - (i + 0.5f) * (buttonHeight + distanceBetweenButtons));
			
			btch.draw(button, startPointX, buttonY, bw, buttonHeight);
			int centredX = (int) (startPointX + bw / 2 - Message.fnt.getBounds(choises[i].k).width / 2);
			
			Message.fnt.draw(btch, choises[i].k, centredX,
					buttonY + buttonHeight / 2 + Message.fnt.getCapHeight() / 2);
			
		}
		
		// Управление
		
		if (!tapped) {
			for (int i = 0; i != choises.length; i++) {
				int buttonY = (int) (startPointY - (i + 0.5f)
						* (buttonHeight + distanceBetweenButtons));
				Sprite boundingBox = new Sprite();
				boundingBox.setPosition(startPointX, buttonY);
				boundingBox.setSize(bw, buttonHeight);
				if (Util.isPressed(boundingBox)) {
					choises[i].v.run();
					this.isMarkedToBeDeleted = true;
				}
			}
		}
		
		tapped = Gdx.input.isTouched();
		
	}
}
