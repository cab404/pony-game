package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Pinnable;
import com.cab404.ponies.Util;

public class Inventory extends Pinnable {
	
	public static Sprite[] boxCorner, boxVert;
	public static Sprite boxInner;
	public static Sprite exitButton;
	public static boolean isInited = false;
	public Muffin owner;
	public Controller lastController;
	private boolean tapped = false;
	private int[] currentDraggingImage = new int[] { -1, -1 };
	/** Size of element **/
	public static int elS = 8;
	
	public Inventory(Muffin inv) {
		owner = inv;
		if (!isInited) {
			isInited = true;
			
			Texture tex = new Texture(Gdx.files.internal("data/images/inventory.png"));
			
			Luna.disposeOnExit.add(tex);
			
			boxCorner = new Sprite[4];
			boxVert = new Sprite[4];
			
			boxInner = new Sprite(new TextureRegion(tex, elS, elS, elS, elS));
			
			boxVert[0] = new Sprite(new TextureRegion(tex, 0, 0, elS, elS));
			boxVert[1] = new Sprite(new TextureRegion(tex, 0, elS * 2, elS, elS));
			boxVert[2] = new Sprite(new TextureRegion(tex, elS * 2, elS * 2, elS, elS));
			boxVert[3] = new Sprite(new TextureRegion(tex, elS * 2, 0, elS, elS));
			
			boxCorner[0] = new Sprite(new TextureRegion(tex, 0, elS, elS, elS));
			boxCorner[1] = new Sprite(new TextureRegion(tex, elS, 0, elS, elS));
			boxCorner[2] = new Sprite(new TextureRegion(tex, elS * 2, elS, elS, elS));
			boxCorner[3] = new Sprite(new TextureRegion(tex, elS, elS * 2, elS, elS));
			
			exitButton = new Sprite(new TextureRegion(tex, elS * 3, 0, elS, elS));
			
		}
		
	}
	
	@Override
	public void draw(Muffin pon, SpriteBatch btch) {
		
		float x = pon.x, y = pon.y + pon.h;
		int sideX = 4;
		int sideY = 4;
		int cellSide = (int) Util.ppc(1);
		int cellFrame = cellSide / elS;
		
		int bw = (cellSide - cellFrame) * sideX + cellFrame;
		int bh = (cellSide - cellFrame) * sideY + cellFrame;
		
		if (pon.pony.animation.isFlipped) x += bw + elS * 2 + pon.w;
		
		Vector2 dot1 = new Vector2(x - bw - elS * 2, y + bh + elS);
		Vector2 dot2 = new Vector2(x - bw - elS * 2, y);
		Vector2 dot3 = new Vector2(x - elS, y);
		Vector2 dot4 = new Vector2(x - elS, y + bh + elS);
		
		btch.draw(boxVert[0], dot1.x, dot1.y);
		btch.draw(boxVert[1], dot2.x, dot2.y);
		btch.draw(boxVert[2], dot3.x, dot3.y);
		btch.draw(boxVert[3], dot4.x, dot4.y);
		
		btch.draw(boxCorner[1], dot1.x + elS, dot1.y, bw, elS);
		btch.draw(boxCorner[2], dot4.x, dot3.y + elS, elS, bh);
		btch.draw(boxCorner[3], dot2.x + elS, dot2.y, bw, elS);
		btch.draw(boxCorner[0], dot2.x, dot2.y + elS, elS, bh);
		
		for (int i = 0; i != sideY; i++)
			for (int j = 0; j != sideX; j++)
				btch.draw(boxInner, dot2.x + elS + cellSide * j - cellFrame * j, dot2.y + elS
						+ cellSide * i - cellFrame * i, cellSide, cellSide);
		
		int dx = 1, dy = 1;
		
		if (currentDraggingImage[0] != -1) {
			dx = (int) owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]].icon
			
			.getX();
			dy = (int) owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]].icon
					.getY();
		}
		
		for (int i = 0; i != sideY; i++)
			for (int j = 0; j != sideX; j++) {
				Item tmp = owner.nowGrabbing[i][j];
				if (tmp != null) {
					int iy = i + 1;
					int ix = j + 1;
					int cx = (ix - 1) * cellSide - (ix - 2) * cellFrame;
					int cy = (iy - 1) * cellSide - (iy - 2) * cellFrame;
					
					tmp.icon.setSize(cellSide - cellFrame * 2, (cellSide - cellFrame * 2)
							* (tmp.icon.getHeight() / tmp.icon.getWidth()));
					tmp.icon.setPosition(cx + elS,
							cy + elS + (cellSide - cellFrame * 2 - tmp.icon.getHeight()) / 2);
				}
			}
		
		exitButton.setPosition(x, bh + y);
		exitButton.setSize(cellSide / 2, cellSide / 2);
		exitButton.draw(btch);
		
		// Управление
		
		owner.inp.isActive = false;
		
		if (Util.isPressed(exitButton) && !tapped || Gdx.input.isKeyPressed(Keys.BACK)
				|| Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			Luna.overlays.removeValue(this, true);
			owner.inp.isActive = true;
		}
		
		if (currentDraggingImage[0] != -1 && tapped)
			owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]].icon.setPosition(
					Gdx.input.getX(), Luna.h - Gdx.input.getY());
		else if (Gdx.input.isTouched() && !tapped) {
			for (int i = 0; i != sideY; i++)
				for (int j = 0; j != sideX; j++)
					if (owner.nowGrabbing[i][j] != null
							&& Util.isPressed(owner.nowGrabbing[i][j].icon))
						currentDraggingImage = new int[] { i, j };
		}
		else if (currentDraggingImage[0] != -1) {
			if (dx < bw + elS && dy < bh + elS) {
				int fy = dx / (bw / sideX);
				int fx = dy / (bh / sideY);
				
				Item tmp = owner.nowGrabbing[fx][fy];
				owner.nowGrabbing[fx][fy] = owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]];
				owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]] = tmp;
			}
			else {
				owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]].owner = null;
				owner.nowGrabbing[currentDraggingImage[0]][currentDraggingImage[1]] = null;
			}
			
			currentDraggingImage = new int[] { -1, -1 };
		}
		
		tapped = Gdx.input.isTouched();
		
		for (int i = 0; i != sideY; i++)
			for (int j = 0; j != sideX; j++) {
				Item tmp = owner.nowGrabbing[i][j];
				if (tmp != null) tmp.icon.draw(btch);
			}
		
	}
}
