package com.cab404.ponies.realizations;

import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.MuffinData;
import com.cab404.ponies.Pony;

public class Projectile extends Muffin {
	
	public int lifeLeft = 200;
	
	public Projectile(Pony reg) {
		super(reg, Controller.getNilController());
		stats.isNPC = true;
		isFlying = true;
	}
	
	public void update(TileMapRenderer map, float time) {
		float vx = vX * time;
		float vy = vY * time;
		
		pony.inMidair(this);
		
		while (vx != 0 || vy != 0) {
			
			if (vy > 0 ? !isUp(map) : !isDown(map)) {
				y += Math.signum(vy) * precision;
				vy -= Math.signum(vy) * precision;
				vy = (float) (Math.round(vy / precision) * precision);
			}
			else {
				vY = 0;
				lifeLeft = 0;
				vy = 0;
			}
			
			if (vx > 0 ? !isRight(map) : !isLeft(map)) {
				x += Math.signum(vx) * precision;
				vx -= Math.signum(vx) * precision;
				vx = (float) (Math.round(vx / precision) * precision);
			}
			else {
				vX = 0;
				lifeLeft = 0;
				vx = 0;
			}
			
			if (vx == 0 && vy == 0) break;
		}
		
		if (!isFlying)
			vY -= isFalling ? MuffinData.G * time : 0;
		
		if (lifeLeft == 0)
			Luna.chars.removeValue(this, true);
		else
			lifeLeft--;
		
		pony.animation.rotation = new Vector2(vX, vY).angle() - 180;
	}
	
	public void setSpeed(float speed) {
		// Нормализуем вектор и умножаем на скорость
		float sum = Math.abs(vX) + Math.abs(vY);
		
		vX = vX / sum * speed;
		vY = vY / sum * speed;
	}
	
}
