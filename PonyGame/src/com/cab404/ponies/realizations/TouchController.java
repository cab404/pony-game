package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Util;

@Deprecated
public class TouchController extends Controller {
	
	boolean isUpLocked = false;
	Sprite left, right, down, up, jump, fly;
	
	public TouchController() {
		
		Texture tex = new Texture(Gdx.files.internal("data/images/buttons.png"));
		right = new Sprite(new TextureRegion(tex, 0, 0, 30, 30));
		
		left = new Sprite(new TextureRegion(tex, 0, 0, 30, 30));
		left.flip(true, false);
		down = new Sprite(new TextureRegion(tex, 0, 0, 30, 30));
		down.rotate90(true);
		up = new Sprite(new TextureRegion(tex, 0, 0, 30, 30));
		up.rotate90(false);
		jump = new Sprite(new TextureRegion(tex, 0, 30, 30, 30));
		fly = new Sprite(new TextureRegion(tex, 30, 0, 30, 30));
		
		int w = Gdx.graphics.getWidth(), h = Gdx.graphics.getHeight();
		float pr = Gdx.graphics.getDensity();
		int rx = w - w / 8, ry = w / 8;
		int rw = (int) (50 * pr), rh = (int) (50 * pr);
		
		jump.setSize(rw, rh);
		fly.setSize(rw, rh);
		left.setSize(rw, rh);
		right.setSize(rw, rh);
		up.setSize(rw, rh);
		down.setSize(rw, rh);
		
		jump.setPosition(w / 11f, h / 8f);
		
		right.setPosition(rx + rw / 2, ry - rh / 2);
		left.setPosition(rx - rw * 1.5f, ry - rh / 2);
		up.setPosition(rx - rw / 2, ry + rh / 2);
		down.setPosition(rx - rw / 2, ry - rh * 1.5f);
		
		jump.setY(up.getY());
		fly.setPosition(jump.getX(), down.getY());
		
		Luna.gui.addAll(new Sprite[] { left, right, jump, up, down, fly });
		Luna.toDispose.add(tex);
	}
	
	@Override
	public float[] getMovement(Muffin phys) {
		float[] toret = new float[] { 0, 0, 0 };
		
		if (Util.isPressed(jump)) {
			if (!isUpLocked && !phys.isFalling) toret[1] += 10;
			isUpLocked = true;
		}
		else
			isUpLocked = false;
		
		if (Util.isPressed(down) && phys.isFlying) toret[1] -= 5;
		if (Util.isPressed(left)) toret[0] -= 5;
		if (Util.isPressed(right)) toret[0] += 5;
		if (Util.isPressed(up)) toret[1] += 5;
		
		if (!phys.stats.canFly) fly.setSize(0, 0);
		
		if (Util.isPressed(fly)) toret[2] = 1;
		
		up.setSize(toret[2] * right.getWidth(), toret[2] * right.getWidth());
		down.setSize(toret[2] * right.getWidth(), toret[2] * right.getWidth());
		jump.setSize(Math.abs(toret[2] - 1) * right.getWidth(),
				Math.abs(toret[2] - 1) * right.getWidth());
		
		return toret;
	}
	
	@Override
	public void dispose() {
		
	}
	
}
