package com.cab404.ponies.realizations.scenes;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Util;
import com.cab404.ponies.realizations.characters.RenaiFoxy;

;

public class SandboxLevel extends Luna {
	
	TileMapRenderer tmr;
	TiledMap map;
	SimpleTileAtlas atlas;
	ObjectOutputStream out;
	ObjectInputStream in;
	
	/**
	 * Just an index of body currently in camera focus.
	 */
	public static int focusID = 0;
	Muffin reina;
	
	@Override
	public void create() {
		
		// Грузим карту
		
		map = TiledLoader.createMap(Gdx.files.internal("data/maps/renai/reinaTest.tmx"));
		
		// Adding some characters
		
		reina = new Muffin(new RenaiFoxy(), Util.getDefaultController());
		
		Vector2 pos = Util.getMapStartPoint(map);
		reina.x = pos.x;
		reina.y = pos.y;
		
		// Inserting them into rendering array
		chars.addAll(new Muffin[] { reina });
		
		// Initializing tile map renderer
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/renai"));
		tmr = new TileMapRenderer(map, atlas, map.tileWidth, map.tileHeight, map.tileWidth,
				map.tileHeight);
		
		toDispose.add(tmr);
		
	}
	
	@Override
	public void render() {
		slowFocus(4, focusID, tmr);
		updateObjects(tmr);
		renderObjects(tmr);
	}
	
	@Override
	public void dispose() {}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {
		// Reloading map tiles due libGDX bug
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/renai"));
		tmr = new TileMapRenderer(map, atlas, map.tileWidth, map.tileHeight, map.tileWidth,
				map.tileHeight);
	}
	
}
