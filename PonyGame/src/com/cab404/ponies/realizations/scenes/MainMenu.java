package com.cab404.ponies.realizations.scenes;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.cab404.ponies.Luna;
import com.cab404.ponies.MuffinData;
import com.cab404.ponies.Util;

public class MainMenu extends Luna {
	
	Sprite logo, bg, play;
	Random rnd = new Random();
	
	public void rePlace() {
		bg.setSize(Math.max(w, h), Math.max(w, h));
		play.setSize(w / 5, w / 5 * 0.56f);
		logo.setSize(w / 2, w / 2 * 0.3f);
		logo.setPosition((w - logo.getWidth()) / 2, h - logo.getHeight());
		play.setPosition((w - play.getWidth()) / 2, h * 2 / 5 - play.getHeight() / 2);
	}
	
	@Override
	public void create() {
		cam.translate(w / 2, h / 2);
		cam.update();
		
		Texture menu = new Texture(Gdx.files.internal("data/images/menu.png"));
		toDispose.add(menu);
		
		bg = new Sprite(new TextureRegion(menu, 0, 0, 100, 100));
		gui.add(bg);
		logo = new Sprite(new TextureRegion(menu, 100, 0, 100, 30));
		gui.add(logo);
		play = new Sprite(new TextureRegion(menu, 0, 100, 50, 28));
		gui.add(play);
		Util.displayMessage(MuffinData.tabun[rnd.nextInt(MuffinData.tabun.length)] + " - лучшая пони!", Integer.MAX_VALUE);
		
	}
	
	@Override
	public void render() {
		rePlace();
		if (Util.isPressed(play)) Luna.chScene(new SandboxLevel());
	}
	
	@Override
	public void dispose() {
		
	}
	
	@Override
	public void pause() {
		
	}
	
	@Override
	public void resume() {
		
	}
	
}
