package com.cab404.ponies.realizations;

import com.cab404.ponies.Stream;

/** Very useful class **/
public class Timer extends Stream {
	
	Runnable toRun = new Runnable() {
		@Override
		public void run() {
			
		}
	};
	
	Runnable onComplete = toRun;
	int leftToGo, delay;
	
	public Timer(Runnable toDo, int repeat, int delay, Runnable onComplete) {
		leftToGo = repeat--;
		this.delay = delay;
		if (toDo != null) toRun = toDo;
		if (onComplete != null) this.onComplete = onComplete;
	}
	
	public Timer(Runnable toDo, int repeat, int delay) {
		this(toDo, repeat, delay, null);
	}
	
	public Timer(Runnable toDo, int repeat) {
		this(toDo, repeat, 100, null);
	}
	
	@Override
	public void update() {
		if (leftToGo != 0 || delay != 0) {
			if (delay != 0)
				delay--;
			else {
				toRun.run();
				leftToGo = leftToGo < 0 ? leftToGo : leftToGo - 1;
			}
		}
		else {
			onComplete.run();
			isFinished = true;
		}
	}
	
	@Override
	public void cancel() {
		isFinished = true;
	}
	
}
