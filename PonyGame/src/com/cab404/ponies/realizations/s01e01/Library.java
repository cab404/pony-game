package com.cab404.ponies.realizations.s01e01;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Dialog;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Util;
import com.cab404.ponies.realizations.Item;
import com.cab404.ponies.realizations.Timer;
import com.cab404.ponies.realizations.characters.BasicPony;
import com.cab404.ponies.realizations.characters.Spike;
import com.cab404.ponies.realizations.scenes.SandboxLevel;

public class Library extends Luna {
	
	TileMapRenderer tmr;
	TiledMap map;
	SimpleTileAtlas atlas;
	public int focusID = 0;
	public SpriteBatch btch;
	public Sprite bg;
	public int mapW, mapH;
	public Muffin twi, spike;
	public Item book;
	
	public int questPhase = -1;
	
	public Controller followStare = new Controller() {
		
		@Override
		public float[] getMovement(Muffin phys) {
			float side = phys.x - twi.x;
			side = (int) Math.signum(side);
			phys.pony.animation.isFlipped = side == -1;
			return new float[] { 0, 0, 0 };
		}
		
		@Override
		public void dispose() {}
		
	};
	
	@Override
	public void create() {
		
		// Loading tile map data
		
		map = TiledLoader.createMap(Gdx.files.internal("data/maps/0101/library.tmx"));
		
		Texture tex = new Texture(Gdx.files.internal("data/maps/0101/canterlot_bg.png"));
		Luna.toDispose.add(tex);
		bg = new Sprite(tex);
		mapH = map.height * map.tileHeight;
		mapW = map.width * map.tileWidth;
		
		btch = new SpriteBatch();
		
		// Adding some characters
		
		spike = new Muffin(new Spike(), Controller.getNilController());
		spike.x = 75 * map.tileWidth;
		spike.y = map.tileHeight * map.height - 18 * map.tileHeight;
		spike.stats.isNPC = false;
		spike.pony.name = "Spike";
		
		twi = new Muffin(new BasicPony("twilight", 49, 41), Controller.getNilController());
		twi.x = 95 * map.tileWidth;
		twi.y = map.tileHeight * map.height - 18 * map.tileHeight;
		twi.stats.isNPC = true;
		twi.pony.name = "Twilight Sparkle";
		
		Texture book_texture = new Texture(Gdx.files.internal("data/images/items/book/book.png"));
		
		book = new Item(
				Item.getStaticAnim(new Sprite(new TextureRegion(book_texture, 0, 0, 17, 7))));
		book.x = 1008;
		book.y = 752;
		
		toDispose.add(book_texture);
		
		// Inserting them into rendering array
		
		chars.addAll(new Muffin[] { twi, spike, book });
		
		// Initializing tile map renderer
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/0101"));
		tmr = new TileMapRenderer(map, atlas, 16, 16, map.tileWidth, map.tileHeight);
		
		toDispose.add(tmr);
		
		Dialog library_dialog = new Dialog(100);
		library_dialog.parseFile(Gdx.files.internal("data/maps/0101/spikeBooks.pgd"));
		int wait = library_dialog.start(new Muffin[] { twi, spike });
		Timer questStart = new Timer(new Runnable() {
			@Override
			public void run() {
				questPhase = 0;
			}
		}, 1, wait);
		streams.add(questStart);
		
	}
	
	@Override
	public void render() {
		
		// Moving camera to currently selected character
		
		if (questPhase == 0) {
			Util.displayMessage("Найди книгу в библиотеке (troll it down like me)", 300);
			questPhase++;
		}
		
		if (questPhase == 1 && book.owner != null && book.owner.equals(spike)) {
			Util.displayMessage(
					"Отлично! Теперь отнеси её Твайлайт. Чтобы открыть инвентарь, нажми кнопку E,"
							+ " если ты играешь на Android, то нажми кнопку с сумкой (которая похожа на письмо). "
							+ " Просто вытащи книгу из инвентаря так, чтобы Твайлайт её подобрала.",
					600);
			questPhase++;
		}
		
		if (questPhase == 2 && book.owner != null && book.owner.equals(twi)) {
			spike.inp.dispose();
			twi.inp.dispose();
			spike.inp = Controller.getNilController();
			
			Dialog dialog = new Dialog(100);
			dialog.parseFile(Gdx.files.internal("data/maps/0101/luna_ll_arive.pgd"));
			dialog.start(new Muffin[] { twi, spike });
			questPhase++;
		}
		
		slowFocus(5, SandboxLevel.focusID, tmr);
		
		btch.setProjectionMatrix(cam.combined);
		
		float delta = Math.abs(w - h * 2);
		delta /= (mapW - w) / w * cam.position.x;
		
		bg.setPosition(cam.position.x - Luna.h + delta, cam.position.y - Luna.h / 2);
		bg.setSize(Luna.h * 2, Luna.h);
		
		btch.begin();
		bg.draw(btch);
		btch.end();
		
		updateObjects(tmr);
		renderObjects(tmr);
		
	}
	
	@Override
	public void dispose() {}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {
		
		// Reloading map tiles due libGDX bug
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/0101"));
		tmr = new TileMapRenderer(map, atlas, map.tileWidth, map.tileHeight, map.tileWidth,
				map.tileHeight);
	}
}
