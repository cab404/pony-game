package com.cab404.ponies.realizations.s01e01;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Dialog;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.realizations.characters.BasicPony;
import com.cab404.ponies.realizations.scenes.SandboxLevel;

public class Running_to_library extends Luna {
	
	TileMapRenderer tmr;
	TiledMap map;
	SimpleTileAtlas atlas;
	public int focusID = 0;
	public SpriteBatch btch;
	public Sprite bg;
	public int mapW, mapH;
	public Muffin twi, p1, p2, p3;
	
	public Controller followStare = new Controller() {
		
		@Override
		public float[] getMovement(Muffin phys) {
			float side = phys.x - twi.x;
			side = (int) Math.signum(side);
			phys.pony.animation.isFlipped = side == -1;
			return new float[] { 0, 0, 0 };
		}
		
		@Override
		public void dispose() {
			
		}
		
	};
	
	@Override
	public void create() {
		
		// Loading tile map data
		
		map = TiledLoader.createMap(Gdx.files.internal("data/maps/0101/running-to-library.tmx"));
		
		Texture tex = new Texture(Gdx.files.internal("data/maps/0101/canterlot_bg.png"));
		Luna.toDispose.add(tex);
		bg = new Sprite(tex);
		mapH = map.height * map.tileHeight;
		mapW = map.width * map.tileWidth;
		
		btch = new SpriteBatch();
		
		// Adding some characters
		
		twi = new Muffin(new BasicPony("twilight", 49, 41), Controller.getNilController());
		twi.x = 85 * map.tileWidth;
		twi.y = map.tileHeight * map.height - 39 * map.tileHeight;
		twi.stats.isNPC = true;
		twi.pony.name = "Twilight Sparkle";
		
		p1 = new Muffin(new BasicPony("colgate", 48, 46), followStare);
		p1.x = 66 * map.tileWidth;
		p1.y = map.tileHeight * map.height - 39 * map.tileHeight;
		p1.stats.isNPC = true;
		p1.pony.name = "Colgate";
		
		p2 = new Muffin(new BasicPony("lemonHearts", 43, 43), followStare);
		p2.x = 68 * map.tileWidth;
		p2.y = map.tileHeight * map.height - 39 * map.tileHeight;
		p2.stats.isNPC = true;
		p2.pony.name = "Lemon Hearts";
		
		p3 = new Muffin(new BasicPony("twinkleshine", 43, 43), followStare);
		p3.x = 67 * map.tileWidth;
		p3.y = map.tileHeight * map.height - 39 * map.tileHeight;
		p3.stats.isNPC = true;
		p3.pony.name = "Twinkleshine";
		
		// Inserting them into rendering array
		
		chars.addAll(new Muffin[] { twi, p2, p3, p1 });
		
		// Initializing tile map renderer
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/0101"));
		tmr = new TileMapRenderer(map, atlas, map.tileWidth, map.tileHeight, map.tileWidth,
				map.tileHeight);
		
		// Story!
		
		Dialog dg = new Dialog(100);
		dg.parseFile(Gdx.files.internal("data/maps/0101/moondancerParty.pgd"));
		dg.start(new Muffin[] { twi, p1, p2, p3 });
		
	}
	
	@Override
	public void render() {
		
		// Moving camera to currently selected character
		
		slowFocus(5, SandboxLevel.focusID, tmr);
		
		btch.setProjectionMatrix(cam.combined);
		
		float delta = Math.abs(w - h * 2);
		delta /= (mapW - w) / w * cam.position.x;
		
		bg.setPosition(cam.position.x - Luna.h + delta, cam.position.y - Luna.h / 2);
		bg.setSize(Luna.h * 2, Luna.h);
		btch.begin();
		bg.draw(btch);
		btch.end();
		
		updateObjects(tmr);
		renderObjects(tmr);
		
	}
	
	@Override
	public void dispose() {
		
		// Everything else being disposed in Luna's methods
		
		tmr.dispose();
	}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {
		
		// Reloading map tiles due libGDX bug
		
		atlas = new SimpleTileAtlas(map, Gdx.files.internal("data/maps/0101"));
		tmr = new TileMapRenderer(map, atlas, map.tileWidth, map.tileHeight, map.tileWidth,
				map.tileHeight);
	}
}
