package com.cab404.ponies.realizations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.cab404.ponies.Controller;
import com.cab404.ponies.Fireball;
import com.cab404.ponies.Luna;
import com.cab404.ponies.Muffin;
import com.cab404.ponies.Util;

public class KeyboardController extends Controller implements Disposable {
	boolean isUpLocked = false;
	boolean isFlyLocked = false;
	boolean isTabLocked = false;
	boolean isFlying = false;
	Inventory curEnv;
	int leftAdditionalJumpingSpeed = 0;
	
	public KeyboardController() {
		identity = "Keyboard";
	}
	
	@Override
	public float[] getMovement(Muffin phys) {
		float[] toret = new float[] { 0, 0, 0 };
		isFlying = phys.isFlying;
		
		if (Gdx.input.isTouched()){
			Projectile mf = Fireball.newFireball();
			Vector2 gCurs = Util.getGlobalCursorPos();
			mf.x = phys.x;
			mf.y = phys.y;
			
			mf.vX = gCurs.x - phys.x;
			mf.vY = gCurs.y - phys.y;
			mf.setSpeed(20f);
			
			Luna.chars.add(mf);
		}
		
		if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			if (phys.isFlying)
				toret[1] += 5;
			else if (!isUpLocked && !phys.isFalling) {
				toret[1] += phys.stats.jumpHeight;
				leftAdditionalJumpingSpeed = phys.stats.additionalJumpSpeedBonusLength;
			}
			else if (isUpLocked && leftAdditionalJumpingSpeed != 0) {
				toret[1] += phys.stats.additionalJumpSpeed;
				leftAdditionalJumpingSpeed--;
			}
			isUpLocked = true;
		}
		else {
			isUpLocked = false;
			leftAdditionalJumpingSpeed = 0;
		}
		
		if (Gdx.input.isKeyPressed(Input.Keys.TAB)) {
			if (!isTabLocked) {
				isTabLocked = true;
				Util.chChar();
			}
		}
		else
			isTabLocked = false;
		
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && phys.stats.canFly) {
			if (!isFlyLocked) {
				isFlyLocked = true;
				isFlying = !isFlying;
			}
		}
		else
			isFlyLocked = false;
		
		toret[2] = isFlying ? 1 : 0;
		
		if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT))
			toret[0] -= 5;
		if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT))
			toret[0] += 5;
		if ((Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.UP))
				&& phys.isFlying) toret[1] -= 5;
		
		if (Gdx.input.isKeyPressed(Input.Keys.E)) {
			curEnv = new Inventory(phys);
			Luna.overlays.add(curEnv);
		}
		return toret;
	}
	
	@Override
	public void dispose() {
		
		if (curEnv != null) {
			curEnv.isMarkedToBeDeleted = true;
		}
		
	}
	
}
