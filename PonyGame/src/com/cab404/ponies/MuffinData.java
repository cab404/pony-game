package com.cab404.ponies;

import java.io.Serializable;

public class MuffinData implements Serializable {
	/**
	 * PonyData v1
	 */
	private static final long serialVersionUID = 1L;
	
	public String name = "";
	public static float G = 0.5f; // pixels per second per second
	public float jumpHeight = 7;
	public float dumpingPerSecond = 0.7f;
	public float airDumpingPerSecond = 0.4f;
	
	public float limitX = 5;
	public float limitY = 15;
	public float normalSpeed = 5;
	public float additionalJumpSpeed = 0.3f;
	public int additionalJumpSpeedBonusLength = 8;
	
	public float x = 0;
	public float y = 0;
	public float vX = 0;
	public float vY = 0;
	public float w = 0;
	public float h = 0;
	
	public float magicLevitationDistance = 0;
	
	public boolean canFly = false;
	public boolean isNPC = false;
	
	public static String[] tabun = new String[] { "Orhideous", "Abaduaber", "Pahtet", "TaLZ",
			"Tomony", "cab404", "DarkKnight", "Stally", "WoodenToaster", "SoundsOfAviators",
			"TheLivingTombstone", "JackleApp", "Lenich", "RaitaFoxy13", "ponyPharmacist", "Дёрпи",
			"Фармацестия", "Принцесса Луна", "Ты", "Renaifoxy", };
	
	public static MuffinData getStandartPonyData(String name) {
		MuffinData p = new MuffinData();
		p.name = name;
		return p;
	}
	
	public void sync(Muffin out) {
		x = out.x;
		y = out.y;
		w = out.w;
		h = out.h;
		vX = out.vX;
		vY = out.vY;
	}
	
}
