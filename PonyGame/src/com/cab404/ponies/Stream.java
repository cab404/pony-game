package com.cab404.ponies;

public abstract class Stream {
	public boolean isFinished = false;
	
	public abstract void update();
	
	public abstract void cancel();
}
