package com.cab404.ponies;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.cab404.ponies.realizations.Choise;
import com.cab404.ponies.realizations.Message;
import com.cab404.ponies.realizations.scenes.MainMenu;
import com.cab404.ponies.realizations.scenes.SandboxLevel;

public abstract class Luna {
	
	/**
	 * Texture storage which used for disposing resources. Just add your
	 * textures inside and Luna will do the rest.
	 */
	
	public static Array<Disposable> disposeOnExit;
	public static Array<Disposable> toDispose;
	/**
	 * Sprites rendered on top of everything.
	 */
	public static Array<Sprite> gui;
	/**
	 * Physical bodies of scene.
	 */
	public static Array<Muffin> chars;
	/**
	 * Parallel tasks.
	 */
	public static Array<Stream> streams;
	/**
	 * Standard font, loaded in Celestia.
	 */
	public static BitmapFont pubf;
	/**
	 * Batch, there UI is being rendered.
	 */
	public static SpriteBatch uiB, overB, charB;
	public static float charBzoom = 2f;
	public static boolean isPaused = false;
	public static OrthographicCamera cam;
	/**
	 * Is debug enabled or not.
	 */
	public static boolean DEBUG = true;
	
	/**
	 * Various windows / popups
	 */
	public static Array<Pinnable> overlays;
	
	/**
	 * Width and height of graphic window.
	 */
	public static float w, h;
	private static boolean isChangeRequested = false;
	private static Luna targetScene;
	private static Array<Class<Luna>> lastScene = new Array<Class<Luna>>();
	
	public Color background = new Color(0, 0.737f, 0.984f, 1);
	
	/**
	 * Blank screen.
	 */
	public static Luna nullScene = new Luna() {
		@Override
		public void create() {}
		
		@Override
		public void render() {}
		
		@Override
		public void dispose() {}
		
		@Override
		public void pause() {}
		
		@Override
		public void resume() {}
		
	};
	
	static {
		currentScene = nullScene;
	}
	
	private static Luna currentScene;
	
	/**
	 * Returns currently rendering scene.
	 * 
	 * @return Current scene
	 */
	public static Luna getCurrentScene() {
		return currentScene;
	}
	
	/**
	 * Changes current scene
	 * 
	 * @param scene
	 *            Target scene
	 */
	@SuppressWarnings("unchecked")
	public static void chScene(Luna scene) {
		isChangeRequested = true;
		lastScene.add((Class<Luna>) scene.getClass());
		targetScene = scene;
	}
	
	/**
	 * Returns to previous scene
	 */
	public static void back() {
		try {
			if (lastScene.size > 1) {
				lastScene.pop();
				chScene(lastScene.pop().newInstance());
			}
			else {
				/* Gdx.app.exit(); */
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Totally disposes everything. It's called by Celestia upon exit.
	 */
	public static void totalDispose() {
		Iterator<Disposable> i = toDispose.iterator();
		
		while (i.hasNext()) {
			i.next().dispose();
			i.remove();
		}
		
		i = disposeOnExit.iterator();
		
		while (i.hasNext()) {
			try {
				i.next().dispose();
			} catch (IllegalArgumentException ex) {}
			i.remove();
		}
		
		Iterator<Muffin> itr = chars.iterator();
		
		while (itr.hasNext()) {
			Muffin tmp = itr.next();
			tmp.dispose();
			itr.remove();
		}
		
		Iterator<Sprite> iter = gui.iterator();
		
		while (iter.hasNext()) {
			iter.next().getTexture().dispose();
			iter.remove();
		}
		
		if (Message.isInited) Message.fnt.dispose();
		Message.isInited = false;
		
		toDispose = null;
		disposeOnExit = null;
		chars = null;
		gui = null;
		SandboxLevel.focusID = 0;
	}
	
	/**
	 * Renders and updates all objects on map. Also rendering map.
	 * 
	 * @param tmr
	 *            Map renderer
	 */
	public static void renderObjects(TileMapRenderer tmr) {
		// Updating physics for characters
		
		Iterator<Muffin> iter = chars.iterator();
		
		// Rendering background layer of map
		
		OrthographicCamera mpRndr = new OrthographicCamera(w / charBzoom, h / charBzoom);
		mpRndr.translate(cam.position.cpy().div(charBzoom));
		mpRndr.update();
		
		tmr.render(mpRndr, new int[] { 0 });
		
		overB.setProjectionMatrix(cam.combined);
		charB.setProjectionMatrix(cam.combined);
		charB.setTransformMatrix(new Matrix4().scl(charBzoom));
		
		charB.begin();
		while (iter.hasNext())
			iter.next().draw();
		charB.end();
		
		tmr.render(mpRndr, new int[] { 1, 2 });
		
		iter = chars.iterator();
		
		overB.begin();
		while (iter.hasNext())
			iter.next().drawOverlays();
		overB.end();
	}
	
	public void updateObjects(TileMapRenderer tmr) {
		if (!isPaused) {
			Iterator<Muffin> iter = chars.iterator();
			
			while (iter.hasNext())
				iter.next().update(tmr, 1);
		}
	}
	
	/**
	 * Takes routine tasks like rendering UI, drawing debug, applying
	 * transitions and changing scenes on itself
	 */
	
	/**
	 * Slowly moves camera by distance / level
	 * 
	 * @param level
	 *            Speed of camera. Distance / level per update
	 * @param ID
	 *            of object in .chars to focus
	 */
	
	public void slowFocus(int level, int ID, TileMapRenderer tmr) {
		
		Vector2 trns = new Vector2(chars.get(ID).x * charBzoom - cam.position.x + chars.get(ID).w
				/ 2 * charBzoom, chars.get(ID).y * charBzoom - cam.position.y + chars.get(ID).h / 2
				* charBzoom);
		
		trns.div(level);
		trns.x = Math.round(trns.x);
		trns.y = Math.round(trns.y);
		
		cam.translate(trns);
		
		int w = (int) cam.viewportWidth;
		int h = (int) cam.viewportHeight;
		int mw = (int) (tmr.getMap().tileWidth * tmr.getMap().width * charBzoom);
		int mh = (int) (tmr.getMap().tileHeight * tmr.getMap().height * charBzoom);
		
		if (cam.position.x > mw - w / 2) cam.position.x = mw - w / 2;
		if (cam.position.y > mh - h / 2) cam.position.y = mh - h / 2;
		if (cam.position.x < w / 2) cam.position.x = w / 2;
		if (cam.position.y < h / 2) cam.position.y = h / 2;
		
		cam.update();
	}
	
	@SuppressWarnings("unchecked")
	public static void Render() {
		
		if ((Gdx.input.isKeyPressed(Input.Keys.BACK) || Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
				&& !isPaused) {
			isPaused = true;
			
			Runnable toMenu = new Runnable() {
				@Override
				public void run() {
					chScene(new MainMenu());
					isPaused = false;
				}
			};
			
			Runnable back = new Runnable() {
				@Override
				public void run() {
					isPaused = false;
				}
			};
			
			Luna.overlays.add(new Choise("Пауза", new KV[] {
					new KV<String, Runnable>("Продолжить", back),
					new KV<String, Runnable>("В меню", toMenu), }));
		}
		else if (!(Gdx.input.isKeyPressed(Input.Keys.BACK) || Gdx.input
				.isKeyPressed(Input.Keys.ESCAPE)))
		
		if (isChangeRequested) {
			isChangeRequested = false;
			currentScene.dispose();
			Celestia.LunaInit();
			currentScene = targetScene;
			Celestia.Resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			currentScene.create();
		}
		
		if (!isPaused) {
			Iterator<Stream> itr = streams.iterator();
			
			while (itr.hasNext()) {
				Stream tmp = itr.next();
				if (tmp.isFinished)
					itr.remove();
				else
					tmp.update();
			}
		}
		
		currentScene.render();
		
		uiB.begin();
		Iterator<Sprite> i = gui.iterator();
		
		while (i.hasNext())
			i.next().draw(uiB);
		
		// Debug displaying
		
		if (DEBUG)
			pubf.drawMultiLine(
					uiB,
					"FPS: " + Gdx.graphics.getFramesPerSecond() + "\n"
							+ "Удаляемых данных загружено: "
							+ (disposeOnExit.size + toDispose.size) + "\n" + "Существ загружено: "
							+ chars.size + "\n" + "Спрайтов GUI загружено: " + gui.size + "\n"
							+ "Окон GUI загружено: " + overlays.size + "\n" + "ПЗ запущено: "
							+ streams.size + "\n\n" + "Операционная система: "
							+ System.getProperty("os.name") + "\n" + "Версия " + Celestia.version,
					pubf.getSpaceWidth(), h - pubf.getCapHeight());
		
		Iterator<Pinnable> itsr = overlays.iterator();
		
		while (itsr.hasNext()) {
			Pinnable tmp = itsr.next();
			if (tmp == null || tmp.isMarkedToBeDeleted)
				itsr.remove();
			else
				tmp.draw(0, 0, uiB);
		}
		uiB.end();
		
	}
	
	/**
	 * Just shortcut to Gdx.app.log, also adds tag itself
	 * 
	 * @param in
	 *            Message for logger
	 */
	public static void log(Object in) {
		if (DEBUG) Gdx.app.log("Princess Luna", in.toString());
	}
	
	/**
	 * Method called upon creation of scene
	 */
	public abstract void create();
	
	/**
	 * Method called upon rendering phase. PLEASE, DO NOT TRY TO RENDER
	 * SOMETHING OUTSIDE IT!
	 */
	public abstract void render();
	
	/**
	 * Method, which should dispose allocated resources of scene. Most of work
	 * is taken by Celestia.LunaInit()
	 */
	public abstract void dispose();
	
	/**
	 * Method called upon pausing game.
	 */
	public abstract void pause();
	
	/**
	 * Method called upon retrieving focus.
	 */
	public abstract void resume();
	
}
