package com.cab404.ponies;

import java.util.Iterator;
import java.util.Scanner;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.OrderedMap;
import com.cab404.ponies.realizations.Timer;
import com.cab404.ponies.realizations.scenes.SandboxLevel;

public class Dialog {
	public Array<KV<String, KV<String, Integer>>> data;
	
	public Dialog(int size) {
		data = new Array<KV<String, KV<String, Integer>>>(size);
	}
	
	public void put(String name, String speech, int readingTime) {
		data.add(new KV<String, KV<String, Integer>>(name, new KV<String, Integer>(speech,
				readingTime)));
	}
	
	public void parseFile(FileHandle input) {
		Scanner in = new Scanner(input.reader("UTF-8"));
		
		String name = "";
		
		while (in.hasNext()) {
			String proc = (in.nextLine());
			if (proc == null || proc == "" || proc.startsWith("//")) continue;
			
			if (proc.startsWith("--"))
				name = proc.substring(2, proc.length() - 1);
			else {
				String command = proc.split(" ", 2)[0];
				
				if (command.equals("say"))
					put(name, proc.split(" ", 3)[2], Integer.parseInt(proc.split(" ")[1]));
				else if (command.equals("gainfocus"))
					put(name, "{GAINFOCUS}", 9000);
				else if (command.equals("wait"))
					put(name, "{WAIT}", Integer.parseInt(proc.split(" ")[1]));
				else if (command.equals("run"))
					put(name, "{RUN}s" + proc.split(" ")[2], Integer.parseInt(proc.split(" ")[1]));
				else if (command.equals("timeback")) {
					int times = 0;
					
					try {
						times = Integer.parseInt(proc.split(" ")[2]);
					} catch (Exception ex) {}
					
					put(name, "{TIMEBACK}", times);
					
				}
				else if (command.equals("position"))
					put(name, "{COORDS}x" + proc.split(" ")[2],
							Integer.parseInt(proc.split(" ")[1]));
				else if (command.equals("gotoscene"))
					put("", "{SCENE}" + proc.split(" ", 2)[1], 0);
				else if (command.equals("getcontrol")) put(name, "{GETCONTROL}", 0);
				
			}
			
		}
		
		in.close();
		data.shrink();
	}
	
	/**
	 * Starts dialog
	 * 
	 * @param actors
	 *            Game objects used in script.
	 * @return Length of dialog in frames. Seconds = Frames / 60
	 */
	public int start(Muffin[] actors) {
		OrderedMap<String, Muffin> actorSheetPre = new OrderedMap<String, Muffin>(actors.length);
		
		Iterator<Muffin> itr = new Array<Muffin>(actors).iterator();
		
		while (itr.hasNext()) {
			Muffin next = itr.next();
			actorSheetPre.put(next.pony.name, next);
		}
		
		final OrderedMap<String, Muffin> actorSheet = actorSheetPre;
		
		int theTimeline = 0;
		Array<Stream> streams = new Array<Stream>(data.size);
		Array<Integer> ctrlZ = new Array<Integer>(data.size);
		
		for (int i = 0; i != data.size; i++) {
			KV<String, KV<String, Integer>> cD = data.get(i);
			final String d1 = cD.k, d2 = cD.v.k;
			final int d3 = cD.v.v;
			final int line = i;
			
			if (d2.startsWith("{RUN}s")) {
				
				streams.add(new Timer(null, 1, theTimeline, new Runnable() {
					@Override
					public void run() {
						Util.runFor(d3, Integer.parseInt(d2.substring(6)), actorSheet.get(d1), null);
					}
				}));
				theTimeline += Math.abs(d3 / Integer.parseInt(d2.substring(6)));
				ctrlZ.add(theTimeline);
				
			}
			else if (d2.startsWith("{WAIT}")) {
				
				theTimeline += d3;
				ctrlZ.add(theTimeline);
				
			}
			else if (d2.startsWith("{GAINFOCUS}"))
				streams.add(new Timer(null, 1, theTimeline, new Runnable() {
					@Override
					public void run() {
						SandboxLevel.focusID = Luna.chars.indexOf(actorSheet.get(d1), false);
					}
				}));
			else if (d2.startsWith("{TIMEBACK}")) {
				
				for (int j = 0; j <= d3; j++)
					ctrlZ.pop();
				theTimeline = ctrlZ.pop();
				
			}
			else if (d2.startsWith("{COORDS}x")) {
				actorSheet.get(d1).x = d3;
				actorSheet.get(d1).y = Integer.parseInt(d2.substring(9));
				
			}
			else if (d2.startsWith("{SCENE}"))
				streams.add(new Timer(null, 1, theTimeline, new Runnable() {
					@Override
					public void run() {
						try {
							Luna.chScene((Luna) Class.forName(
									"com.cab404.ponies.realizations." + d2.substring(7))
									.newInstance());
						} catch (Exception ex) {
							throw new GdxRuntimeException("Parsing error - no scene with name "
									+ d2.substring(7) + " found for gotoscene on command " + line);
						}
					}
				}));
			else if (d2.startsWith("{GETCONTROL}"))
				streams.add(new Timer(null, 1, theTimeline, new Runnable() {
					@Override
					public void run() {
						actorSheet.get(d1).inp = Util.getDefaultController();
					}
				}));
			else {
				
				streams.add(new Timer(null, 1, theTimeline, new Runnable() {
					@Override
					public void run() {
						if (d1.length() == 0)
							Util.displayMessage(d2, d3);
						else
							Util.addMessage(actorSheet.get(d1), d2, d3);
					}
				}));
				theTimeline += d3;
				ctrlZ.add(theTimeline);
			}
			
		}
		Luna.streams.addAll(streams);
		return theTimeline;
		
	}
}
