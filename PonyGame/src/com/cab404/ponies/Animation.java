package com.cab404.ponies;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public class Animation implements Disposable {
	public Sprite[] frames;
	private Sprite drawable;
	public int length;
	private int index;
	public boolean isPaused = false;
	public int sIndex, fIndex;
	public boolean isFlipped = false;
	public int delay;
	private int delayGone;
	private int width = 0, height = 0;
	public int moveX, moveY;
	public float rotation = 0;
	
	public Animation(Sprite[] frm, int delay) {
		frames = frm;
		length = frm.length;
		sIndex = 0;
		fIndex = length - 1;
		this.delay = delay;
		delayGone = 0;
		drawable = frames[0];
		index = 0;
		width = (int) frm[0].getWidth();
		height = (int) frm[0].getHeight();
	}
	
	public void setCoords(float x, float y) {
		for (int i = 0; i != length; i++)
			frames[i].setPosition(x, y);
	}
	
	public void draw(SpriteBatch btch, float x, float y) {
		setCoords(x - moveX, y - moveY);
		draw(btch);
	}
	
	public void draw(SpriteBatch btch) {
		drawable.flip(isFlipped && !drawable.isFlipX(), false);
		drawable.setRotation(rotation);
		drawable.draw(btch);
	}
	
	public void update() {
		delayGone++;
		
		if (delayGone > delay) {
			delayGone = 0;
			if (!isPaused) {
				index++;
				
				if (index > fIndex || index < sIndex) index = sIndex;
			}
			drawable.set(frames[index]);
		}
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		if (index < length && index >= 0) this.index = index;
	}
	
	@Override
	public void dispose() {
		for (int i = 0; i != length; i++)
			frames[i].getTexture().dispose();
		
	}
}
