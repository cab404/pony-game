package com.cab404.ponies;

public class KV<K, V> {
	public K k;
	public V v;
	
	public KV(K key, V value) {
		k = key;
		v = value;
	}
}
