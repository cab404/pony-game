package com.cab404.ponies;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.OrderedMap;
import com.cab404.ponies.realizations.DPadController;
import com.cab404.ponies.realizations.KeyboardController;
import com.cab404.ponies.realizations.Message;
import com.cab404.ponies.realizations.Timer;
import com.cab404.ponies.realizations.Transition;
import com.cab404.ponies.realizations.scenes.SandboxLevel;

/** Just a bunch of useful static methods **/
public class Util {
	
	public static int getTileByCoord(TiledMap map, float x, float y) {
		x = (float) Math.floor(x / map.tileWidth);
		y = (float) Math.floor(y / map.tileHeight);
		y = map.height - y - 1;
		
		try {
			return map.layers.get(0).tiles[(int) x][(int) y];
		} catch (IndexOutOfBoundsException ex) {
			return -1;
		}
	}
	
	public static boolean isPressed(Sprite spr) {
		for (int i = 0; i <= 9; i++)
			if (Gdx.input.isTouched(i)
					&& spr.getBoundingRectangle().contains(Gdx.input.getX(i),
							Gdx.graphics.getHeight() - Gdx.input.getY(i))) return true;
		return false;
	}
	
	/**
	 * Проверяет, нажат ли нарисованный в charB спрайт
	 * 
	 * @param spr
	 * @return
	 */
	public static boolean isPressedGlobal(Sprite spr) {
		Vector2 curCoords = getMousePosition();
		curCoords.mul(Luna.charBzoom);
		curCoords.add(Luna.cam.position.x, Luna.cam.position.y);
		return spr.getBoundingRectangle().contains(curCoords.x, curCoords.y);
	}
	
	public static boolean isPressedGlobal(Muffin spr) {
		Sprite BB = new Sprite();
		BB.setSize(spr.w, spr.h);
		BB.setPosition(spr.x, spr.y);
		return isPressedGlobal(BB);
	}
	
	/**
	 * Change character to next in Luna's chars.
	 */
	public static void chChar() {
		int lastIndex = SandboxLevel.focusID;
		SandboxLevel.focusID = SandboxLevel.focusID + 1 > Luna.chars.size - 1 ? 0
				: SandboxLevel.focusID + 1;
		
		Controller tmp = Luna.chars.get(lastIndex).inp;
		Luna.chars.get(lastIndex).inp = Luna.chars.get(SandboxLevel.focusID).inp;
		Luna.chars.get(SandboxLevel.focusID).inp = tmp;
		
		if (Luna.chars.get(SandboxLevel.focusID).stats.isNPC) chChar();
	}
	
	/**
	 * Finds which finger is tapping given sprite
	 * 
	 * @param Sprite
	 *            to check
	 * @return spr ID of finger
	 * 
	 */
	
	public static int getPressed(Sprite spr) {
		for (int i = 0; i <= 9; i++)
			if (Gdx.input.isTouched(i)
					&& spr.getBoundingRectangle().contains(Gdx.input.getX(i),
							Gdx.graphics.getHeight() - Gdx.input.getY(i))) return i;
		return -1;
	}
	
	/**
	 * Gets the tile id by global coordinates, if it is outside of map's bounds
	 * returns -1
	 * 
	 * @param map
	 * @param x
	 * @param y
	 * @return
	 */
	public static int getTileByCoord(TileMapRenderer map, float x, float y) {
		x = map.getCol((int) x);
		y = map.getRow((int) y);
		y = map.getMap().height - y - 1;
		
		if (y < map.getMap().height && x < map.getMap().width && y >= 0 && x >= 0)
			return map.getMap().layers.get(1).tiles[(int) y][(int) x];
		else
			return -1;
	}
	
	public static boolean isSolid(TileMapRenderer map, float x, float y) {
		int id = getTileByCoord(map, x, y);
		return id == -1 ? true : id == 0 ? false : true;
	}
	
	public static int performTransition(OrderedMap<String, Float> in, Sprite spr) {
		Transition trns = new Transition(in, spr);
		Luna.streams.add(trns);
		return Luna.streams.indexOf(trns, true);
	}
	
	public static void cancelStream(int id) {
		if (id >= 0 && id < Luna.streams.size) {
			Stream trans = Luna.streams.get(id);
			if (trans != null) trans.cancel();
		}
	}
	
	public static void addMessage(final Muffin target, String message, int time) {
		final Message mes = new Message(message);
		target.overlays.add(mes);
		
		Timer tim = new Timer(null, 1, time, new Runnable() {
			@Override
			public void run() {
				target.overlays.removeValue(mes, true);
			}
		});
		
		Luna.streams.add(tim);
	}
	
	/**
	 * Starts cascade of messages. Perfect for dialogs.
	 * 
	 * @param cascade
	 *            Ordered map of messages and PBodies whom saying message.
	 */
	@Deprecated
	public static void MessageCascade(final Array<KV<Muffin, String>> cascade) {
		recursiveDialog(cascade, 0);
	}
	
	private static void recursiveDialog(final Array<KV<Muffin, String>> cascade, final int currentID) {
		addMessage(cascade.get(currentID).k, cascade.get(currentID).v, 100);
		
		if (currentID != cascade.size - 1) {
			Timer next = new Timer(null, 1, 110, new Runnable() {
				@Override
				public void run() {
					recursiveDialog(cascade, currentID + 1);
				}
			});
			
			Luna.streams.add(next);
		}
	}
	
	/**
	 * @return Default controller for current device
	 */
	public static Controller getDefaultController() {
		if (Gdx.app.getType() == ApplicationType.Android)
			return new DPadController();
		else
			return new KeyboardController();
	}
	
	public static void runFor(int dltaX, final int speed, final Muffin target,
			final Runnable onComplete) {
		final int deltaX = (int) (dltaX);
		final float backupMaxX = target.stats.limitX;
		Timer next = new Timer(new Runnable() {
			@Override
			public void run() {
				target.vX += speed * Math.signum(deltaX);
				target.stats.limitX = speed;
			}
		}, Math.abs(deltaX / speed), 2, new Runnable() {
			@Override
			public void run() {
				if (onComplete != null) onComplete.run();
				target.stats.limitX = backupMaxX;
			}
		});
		
		Luna.streams.add(next);
	}
	
	/**
	 * 
	 * Returns split texture
	 * 
	 * @param in
	 *            Texture to split
	 * @param x
	 *            Start point x
	 * @param y
	 *            Start point y
	 * @param w
	 *            Width of one image
	 * @param h
	 *            Height of one image
	 * @param num
	 *            Number of images
	 * @return array of sprites
	 */
	
	public static Sprite[] split(Texture in, int x, int y, int w, int h, int num) {
		Sprite[] toRet = new Sprite[num];
		int breakVar = 0;
		
		for (int j = 0; j != in.getHeight() / h; j++) {
			for (int i = 0; i != in.getWidth() / w; i++) {
				
				toRet[breakVar] = new Sprite(new TextureRegion(in, x + w * i, y + h * j, w, h));
				breakVar++;
				if (breakVar == num) break;
				
			}
		}
		
		return toRet;
	}
	
	public static void displayMessage(String str, int time) {
		final Message toAdd = new Message(str);
		Luna.overlays.add(toAdd);
		
		Timer tim = new Timer(null, 1, time, new Runnable() {
			@Override
			public void run() {
				toAdd.isMarkedToBeDeleted = true;
			}
		});
		Luna.streams.add(tim);
	}
	
	/**
	 * Converts inches into pixels
	 * 
	 * @param inch
	 * @return
	 */
	public static float ppi(float inch) {
		int ppi = (int) (160 / (1 / Gdx.graphics.getDensity()));
		return ppi * inch;
	}
	
	/**
	 * Converts centimeters into pixels
	 * 
	 * @param sm
	 * @return
	 */
	public static float ppc(float sm) {
		return ppi(sm) / 2.54f;
	}
	
	/**
	 * Generates updating pattern for Luna
	 * 
	 * @param ups
	 *            Updates per second. Second is 60 frames
	 * @return updating pattern for Luna
	 */
	public static void setUpdatePattern(int ups) {
		boolean[] toRet = new boolean[60];
		float wait = 60f / ups;
		float curr = 0;
		
		for (int i = 0; i != 60; i++)
			toRet[i] = false;
		
		while ((int) curr < 60) {
			toRet[(int) curr] = true;
			curr += wait;
		}
		
	}
	
	public static Runnable getNullRunnable() {
		return new Runnable() {
			@Override
			public void run() {
				
			}
		};
	}
	
	public static Vector2 getMousePosition(int pointer) {
		return new Vector2(Gdx.input.getX(pointer), Luna.h - Gdx.input.getY(pointer));
	}
	
	public static Vector2 getMousePosition() {
		return new Vector2(Gdx.input.getX(), Luna.h - Gdx.input.getY());
	}
	
	public static Vector2 getMapStartPoint(TiledMap map) {
		String coords = map.properties.get("startPoint");
		int x = Integer.parseInt(coords.split(",")[0]);
		int y = Integer.parseInt(coords.split(",")[1]);
		
		return new Vector2(x * map.tileWidth, (map.height - y - 1) * map.tileHeight);
	}
	
	/**
	 * @return Положение курсора в charB
	 */
	public static Vector2 getGlobalCursorPos() {
		if (Gdx.input.isTouched()) {
			Vector2 camPos = new Vector2(Luna.cam.position.x, Luna.cam.position.y);
			Vector2 mousePos = new Vector2(Gdx.input.getX(), Luna.h - Gdx.input.getY());
			camPos.add(-Luna.w / 2, -Luna.h / 2);
			mousePos.add(camPos);
			
			mousePos.div(Luna.charBzoom);
			
			return mousePos;
		}
		return new Vector2(0, 0);
	}
	
}
