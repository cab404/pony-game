package com.cab404.ponies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.cab404.ponies.realizations.Item;
import com.cab404.ponies.realizations.Projectile;

public class Fireball extends Projectile{
	
	public Fireball(Pony reg) {
		super(reg);
		// TODO Auto-generated constructor stub
	}

	static Sprite spr;

	public static Fireball newFireball() {
		
		if (spr == null){
			spr = new Sprite(new Texture(Gdx.files.internal("data/images/projectiles/fireball.png")));
			spr.setSize(32, 16);
		}
		
		Fireball fb = new Fireball(Item.getStaticAnim(spr));
		
		return fb;
	}
	
}
