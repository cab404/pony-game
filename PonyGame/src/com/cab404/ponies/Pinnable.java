package com.cab404.ponies;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Pinnable {
	public abstract void draw(Muffin pon, SpriteBatch btch);
	
	public void draw(int x, int y, SpriteBatch btch) {
		Muffin mf = new Muffin(0, 0);
		mf.x = x;
		mf.y = y;
		mf.pony = Pony.getNullPony();
		mf.pony.name = "Принцесса Луна";
		Sprite nl = new Sprite();
		nl.setSize(0, 0);
		nl.setPosition(x, y);
		mf.pony.animation = new Animation(new Sprite[] { nl }, 0);
		mf.pony.animation.isFlipped = true;
		draw(mf, btch);
	}
	
	public boolean isActive = true, isMarkedToBeDeleted = false;
}
