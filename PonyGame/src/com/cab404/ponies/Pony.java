package com.cab404.ponies;

/** This is behavior class for Animation **/
public abstract class Pony {
	
	public String name = "";
	
	public Animation animation;
	
	public void updFlip(float vX) {
		if (vX > 0) animation.isFlipped = true;
		if (vX < 0) animation.isFlipped = false;
	}
	
	public void init(Muffin itself) {
		
	}
	
	public abstract void inMidair(Muffin phys);
	
	public abstract void onWalking(Muffin phys);
	
	public abstract void onStanding(Muffin phys);
	
	public abstract void whileFlying(Muffin phys);
	
	public abstract void jumpUp(Muffin phys);
	
	public abstract void fallDown(Muffin phys);
	
	public static Pony getNullPony() {
		return new Pony() {
			
			@Override
			public void inMidair(Muffin phys) {}
			
			@Override
			public void onWalking(Muffin phys) {}
			
			@Override
			public void onStanding(Muffin phys) {}
			
			@Override
			public void whileFlying(Muffin phys) {}
			
			@Override
			public void jumpUp(Muffin phys) {}
			
			@Override
			public void fallDown(Muffin phys) {}
			
		};
	}
	
}
