package com.cab404.ponies;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.cab404.ponies.realizations.scenes.MainMenu;

class Celestia implements ApplicationListener {
	
	public static final String version = "0.1.1 alpha";
	public static int ppi;
	
	private void init() {
		
		FreeTypeFontGenerator ftf = new FreeTypeFontGenerator(
				Gdx.files.internal("data/font/5x8.ttf"));
		Luna.pubf = ftf.generateFont(8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz"
				+ "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
				+ "1234567890-=!\"№;%:?*()_+\\/|<>.,'[]{}", false);
		ftf.dispose();
		
		Luna.pubf.setColor(1, 1, 1, 1);
		
		Gdx.input.setCatchBackKey(true);
		
		Texture.setEnforcePotImages(false);
		
		Luna.disposeOnExit = new Array<Disposable>(100);
		Luna.uiB = new SpriteBatch();
		Luna.charB = new SpriteBatch();
		Luna.overB = new SpriteBatch();
		Luna.disposeOnExit.add(Luna.charB);
		Luna.disposeOnExit.add(Luna.overB);
		Luna.disposeOnExit.add(Luna.uiB);
		Luna.disposeOnExit.add(Luna.pubf);
		Gdx.graphics.setTitle("Pony Game " + version);
	}
	
	/**
	 * Initializes Luna for next scene
	 */
	public static void LunaInit() {
		Luna.toDispose = new Array<Disposable>(300);
		Luna.gui = new Array<Sprite>(50);
		Luna.chars = new Array<Muffin>(20);
		Luna.streams = new Array<Stream>(200);
		Luna.overlays = new Array<Pinnable>(200);
		Luna.cam = new OrthographicCamera(Luna.w, Luna.h);
		Luna.w = Gdx.graphics.getWidth();
		Luna.h = Gdx.graphics.getHeight();
		
	}
	
	@Override
	public void create() {
		init();
		Luna.chScene(new MainMenu());
	}
	
	@Override
	public void dispose() {
		Luna.getCurrentScene().dispose();
		if (!isErrorOccured) Luna.totalDispose();
	}
	
	private static boolean isErrorOccured = false;
	private static String errorMessage = "";
	
	@Override
	public void render() {
		try {
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			
			Color bg_color = Luna.getCurrentScene().background;
			
			Gdx.gl.glClearColor(bg_color.r, bg_color.g, bg_color.b, bg_color.a);
			if (!isErrorOccured)
				Luna.Render();
			else {
				SpriteBatch btch = new SpriteBatch();
				btch.setTransformMatrix(new Matrix4());
				btch.begin();
				Luna.pubf.setColor(Color.WHITE);
				Luna.pubf.drawMultiLine(btch, errorMessage, 10, Gdx.graphics.getHeight() - 10);
				btch.end();
				
			}
		} catch (Exception ex) {
			isErrorOccured = true;
			String stack = ex.toString() + "\n";
			String tab = "";
			ex.printStackTrace();
			for (StackTraceElement e : ex.getStackTrace()) {
				stack += tab + "В файле " + e.getFileName() + " в методе " + e.getMethodName()
						+ " на строке " + e.getLineNumber() + "\n";
				tab += "  ";
			}
			Luna.getCurrentScene().background = Color.BLACK;
			errorMessage = "Принцесса Селестия нашла ошибку в коде, и вынуждена остановаить игру: \n\n"
					+ stack
					+ "\n\n Пожалуйста, оставьте сообщение об ошибке вместе со скриншотом на \n bitbucket.org/Cabinet404/pony-game/issues";
		}
	}
	
	@Override
	public void resize(int width, int height) {
		Resize(width, height);
	}
	
	public static void Resize(int width, int height) {
		Util.setUpdatePattern(60);
		Luna.charBzoom = Util.ppc(0.5f) / 16;
		Luna.w = width;
		Luna.h = height;
		
		Luna.cam = new OrthographicCamera(Luna.w, Luna.h);
		
		Luna.cam.setToOrtho(false, Luna.w, Luna.h);
		Luna.cam.update();
		
		Luna.charB.setProjectionMatrix(Luna.cam.combined);
		Luna.charB.setTransformMatrix(new Matrix4().scl(Luna.charBzoom));
		Luna.overB.setProjectionMatrix(Luna.cam.combined);
		Luna.overB.setTransformMatrix(new Matrix4());
		
		Luna.disposeOnExit.removeValue(Luna.uiB, false);
		Luna.uiB.dispose();
		Luna.uiB = new SpriteBatch();
		Luna.disposeOnExit.add(Luna.uiB);
	}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {
		Luna.getCurrentScene().resume();
	}
}
