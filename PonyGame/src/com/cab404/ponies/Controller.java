package com.cab404.ponies;

public abstract class Controller extends Object {
	protected String identity = "nil";
	public boolean isActive = true;
	
	public static Controller getNilController() {
		return new Controller() {
			@Override
			public float[] getMovement(Muffin phys) {
				return new float[] { 0, 0, phys.isFlying ? 1 : 0 };
			}
			
			@Override
			public void dispose() {}
		};
	}
	
	public boolean equals(Controller toCompare) {
		return identity == toCompare.identity;
	}
	
	public abstract void dispose();
	
	public float[] getMovementIfAvalible(Muffin phys) {
		if (isActive)
			return getMovement(phys);
		else
			return new float[] { 0, 0, 0 };
	}
	
	public abstract float[] getMovement(Muffin phys);
}
