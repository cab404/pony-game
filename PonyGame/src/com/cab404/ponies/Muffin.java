package com.cab404.ponies;

import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.cab404.ponies.realizations.Item;

public class Muffin extends Object implements Disposable {
	public static int checkPrecision = 2;
	
	public boolean isFalling = true, isFlying = false;
	public float w, h, x, y, vX, vY;
	public Pony pony;
	public MuffinData stats;
	public Item[][] nowGrabbing;
	public float precision = 0.1f;
	public Array<Pinnable> overlays;
	public Controller inp;
	private double ID;
	
	public float[] lastMovement;
	public boolean moved = false;
	
	public Muffin(Pony reg, Controller cont) {
		w = reg.animation.getWidth();
		h = reg.animation.getHeight();
		stats = MuffinData.getStandartPonyData(reg.name);
		vX = vY = 0;
		inp = cont;
		pony = reg;
		overlays = new Array<Pinnable>(16);
		nowGrabbing = new Item[4][4];
		ID = System.nanoTime();
		reg.init(this);
	}
	
	/**
	 * Creates null body!
	 * 
	 * @param width
	 * @param height
	 */
	public Muffin(int width, int height) {
		w = width;
		h = height;
		vX = vY = 0;
		x = y = 0;
		overlays = new Array<Pinnable>();
		ID = System.nanoTime();
	}
	
	public boolean equals(Muffin in) {
		return in != null && in.ID == ID;
	}
	
	@Override
	public String toString() {
		return pony.name + " : " + x + " " + y;
	}
	
	@Override
	public void dispose() {
		pony.animation.dispose();
		overlays = null;
	}
	
	protected boolean isDown(TileMapRenderer map) {
		boolean isIt = false;
		
		for (int i = 0; i < w; i += map.getMap().tileWidth / checkPrecision > w - i - 1 ? w - i - 1
				: map.getMap().tileWidth / checkPrecision) {
			if (Util.isSolid(map, x + i, y - precision)) {
				isIt = true;
				break;
			}
			
			if (i == w - 1) break;
		}
		isFalling = !isIt;
		vY = isFalling ? vY : 0;
		return isIt;
	}
	
	protected boolean isLeft(TileMapRenderer map) {
		boolean isIt = false;
		
		for (int i = 0; i < h; i += map.getMap().tileHeight / checkPrecision > h - i - 1 ? h - i
				- 1 : map.getMap().tileHeight / checkPrecision) {
			if (Util.isSolid(map, x - precision, y + i)) {
				isIt = true;
				break;
			}
			if (i == h - 1) break;
		}
		return isIt;
	}
	
	protected boolean isUp(TileMapRenderer map) {
		boolean isIt = false;
		for (int i = 0; i < w; i += map.getMap().tileWidth / checkPrecision > w - i - 1 ? w - i - 1
				: map.getMap().tileWidth / checkPrecision) {
			if (Util.isSolid(map, x + i, y + h + precision)) {
				isIt = true;
				break;
			}
			
			if (i == w - 1) break;
		}
		return isIt;
	}
	
	protected boolean isRight(TileMapRenderer map) {
		boolean isIt = false;
		for (int i = 0; i < h; i += map.getMap().tileHeight / checkPrecision > h - i - 1 ? h - i
				- 1 : map.getMap().tileHeight / checkPrecision) {
			if (Util.isSolid(map, x + w + precision, y + i)) {
				isIt = true;
				break;
			}
			if (i == h - 1) break;
			
		}
		return isIt;
	}
	
	public void update(TileMapRenderer map) {
		update(map, 1);
	}
	
	public void update(TileMapRenderer map, float time) {
		
		float[] input = inp.getMovementIfAvalible(this);
		
		if (lastMovement != input) {
			lastMovement = input;
			moved = true;
		}
		else {
			moved = false;
		}
		
		vX += input[0];
		vY += input[1];
		
		isFlying = input[2] == 1;
		
		// Проверка на превышение лимита скорости.
		
		vX = (Math.abs(vX) > stats.limitX ? Math.signum(vX) * stats.limitX : vX);
		vY = (Math.abs(vY) > (isFlying ? stats.limitX : stats.limitY) ? Math.signum(vY)
				* (isFlying ? stats.limitX : stats.limitY) : vY);
		
		x = (float) ((int) Math.round(x / precision) * precision);
		y = (float) ((int) Math.round(y / precision) * precision);
		
		vX = (float) ((int) Math.round(vX / precision) * precision);
		vY = (float) ((int) Math.round(vY / precision) * precision);
		
		float vx = vX * time;
		float vy = vY * time;
		
		// Применение скорости
		
		while (vx != 0 || vy != 0) {
			
			if (vy > 0 ? !isUp(map) : !isDown(map)) {
				y += Math.signum(vy) * precision;
				vy -= Math.signum(vy) * precision;
				vy = (float) (Math.round(vy / precision) * precision);
			}
			else {
				vY = 0;
				vy = 0;
			}
			
			if (vx > 0 ? !isRight(map) : !isLeft(map)) {
				x += Math.signum(vx) * precision;
				vx -= Math.signum(vx) * precision;
				vx = (float) (Math.round(vx / precision) * precision);
			}
			else {
				vX = 0;
				vx = 0;
			}
			
			if (vx == 0 && vy == 0) break;
			
		}
		
		isDown(map);
		
		// Настройка анимации
		
		if (!isFlying) {
			if (isFalling) {
				if (vY == 0) pony.inMidair(this);
				if (vY < 0) pony.fallDown(this);
				if (vY > 0) pony.jumpUp(this);
				
			}
			else if (Math.abs(vX) > 0)
				pony.onWalking(this);
			else
				pony.onStanding(this);
		}
		else
			pony.whileFlying(this);
		
		// Применение гравитации и трения
		
		if (isFalling)
			vX -= stats.airDumpingPerSecond * time <= Math.abs(vX) ? Math.signum(vX)
					* stats.airDumpingPerSecond * time : vX;
		else {
			vX -= stats.dumpingPerSecond <= Math.abs(vX) ? Math.signum(vX) * stats.dumpingPerSecond * time
					: vX;
		}
		if (!isFlying)
			vY -= isFalling ? MuffinData.G * time : 0;
		else
			vY -= stats.dumpingPerSecond <= Math.abs(vY) ? Math.signum(vY) * stats.dumpingPerSecond * time
					: vY;
		
		pony.animation.update();
		onUpdate();
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, w, h);
	}
	
	/**
	 * If you want object to do something special - override this.
	 */
	public void onUpdate() {}
	
	public void draw() {
		pony.animation.draw(Luna.charB, x, y);
	}
	
	public void drawOverlays() {
		
		Iterator<Pinnable> itr = overlays.iterator();
		
		while (itr.hasNext()) {
			Pinnable tmp = itr.next();
			if (tmp.isMarkedToBeDeleted)
				itr.remove();
			else
				tmp.draw(this, Luna.overB);
		}
	}
	
}
