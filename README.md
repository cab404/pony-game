Pony Game
=========
Игра-платформер, поддерживаемая сообществом Everypony.ru

Распространяется по лицензии GPLv2

http://www.gnu.org/licenses/gpl-2.0.html

Используется библиотека libGDX и арт из Desktop Ponies
