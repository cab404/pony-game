package com.cab404.ponies;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "PonyGame";
		cfg.useGL20 = true;
		cfg.width = 800;
		cfg.height = 600;
		// cfg.addIcon("data/images/icon32.png", FileType.Classpath);
		// cfg.addIcon("data/images/icon64.png", FileType.Classpath);
		cfg.addIcon("data/images/icon128.png", FileType.Classpath);
		
		new LwjglApplication(new Celestia(), cfg);
	}
}
